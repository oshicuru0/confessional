package app.or.confessional.worstvoted;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.utils.AdMobHelper;

@EFragment(R.layout.fragment_worst_voted)
public class WorstVotedFragment extends Fragment implements WorstVotedAdapter.OnChatItemClick {

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public ProgressBar progressLoader;

    @ViewById
    public AdView adView;

    public WorstVotedAdapter worstVotedAdapter;

    public WorstVotedFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        worstVotedAdapter = new WorstVotedAdapter();
        worstVotedAdapter.setOnChatItemClick(this);
        recycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        recycler.setAdapter(worstVotedAdapter);
        getFeeds();

        AdMobHelper.showAdd(adView);
    }

    @Override
    public void onItemClick(int position) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Intent intent = new Intent(mainActivity, CommentsActivity_.class);
        intent.putExtra("type", "WorstVoted");
        intent.putExtra("confessionalId", worstVotedAdapter.getConfessions().get(position).getId());
        mainActivity.startActivity(intent);
    }

    public void getFeeds() {
        progressLoader.setVisibility(View.VISIBLE);
        FeedHelper.getAllDownVote(new FeedHelper.FeedHandler() {
            @Override
            public void onResponse(Object response) {
                if (response == null || !(response instanceof List)) {
                    return;
                }

                final MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
                    return;
                }
                progressLoader.setVisibility(View.GONE);
                worstVotedAdapter.setConfessions((List<Confession>) response);
            }
        });
    }
}
