package app.or.confessional.myconfessions;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.feed.FeedAdapter;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.utils.ConfessionalShowcaseHelper;
import app.or.confessional.widget.ConfessDialog;

@EFragment(R.layout.fragment_my_confessions)
public class MyConfessionsFragment extends Fragment implements FeedAdapter.OnChatItemClick{

    @ViewById
    public RecyclerView recycler;

    private FeedAdapter feedAdapter;

    @ViewById
    public ProgressBar progressLoader;

    @ViewById
    public View placeholder;

    @ViewById
    public View root;

    public MyConfessionsFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mainActivity.setMainButtonClick(new MainActivity.MainButtonClick() {
            @Override
            public void onSort() {
                onConfess();
            }
        });

        feedAdapter = new FeedAdapter(mainActivity);
        feedAdapter.setOnChatItemClick(this);
        recycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        recycler.addItemDecoration(new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL));
        recycler.setAdapter(feedAdapter);
        getFeeds();
        showForAdd();
    }

    @UiThread(delay = 120)
    public void showForAdd() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessionalShowcaseHelper.showAddNewConfessions(mainActivity);
    }

    @Click(resName = "confess")
    protected void onConfess() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessDialog.createDialog(mainActivity).setListener(new ConfessDialog.ButtonClick() {
            @Override
            public void onYes(String message) {
                Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

    @Override
    public void onItemClick(int position) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Intent intent = new Intent(mainActivity, CommentsActivity_.class);
        intent.putExtra("type", "MyConfessions");
        intent.putExtra("confessionalId", ((Confession)feedAdapter.getConfessions().get(position)).getId());
        mainActivity.startActivity(intent);
    }

    public void getFeeds() {
        progressLoader.setVisibility(View.VISIBLE);
        FeedHelper.getAllMyConfessions(new FeedHelper.FeedHandler() {
            @Override
            public void onResponse(Object response) {
                if (response == null || !(response instanceof List)) {
                    return;
                }

                final MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
                    return;
                }

                if (((List) response).size() == 0) {
                    placeholder.setVisibility(View.VISIBLE);
                } else {
                    placeholder.setVisibility(View.GONE);
                }

                progressLoader.setVisibility(View.GONE);
                feedAdapter.setConfessions((List<Confession>) response);
            }
        });
    }
}
