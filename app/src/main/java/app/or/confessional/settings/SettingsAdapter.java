package app.or.confessional.settings;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.widget.ConfessionalTextView;


public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<RecyclerView.ViewHolder> confessions = new ArrayList<>();

    public SettingsAdapter() {
        this.confessions = new ArrayList<>();
    }

    public void setConfessions(List<RecyclerView.ViewHolder> confessions) {
        this.confessions = confessions;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SettingsHolder0(View.inflate(parent.getContext(), R.layout.best_voted_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {
        System.out.println("TEST_TEST: " + holder.getClass().getSimpleName());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public static class SettingsHolder0 extends RecyclerView.ViewHolder {
        public View itemView;
        public View upVote, downVote;
        public ConfessionalTextView downVoteLabel, upVoteLabel, content, comments, time;

        public SettingsHolder0(View view) {
            super(view);
            itemView = view;
            upVote = view.findViewById(R.id.upVote);
            downVote = view.findViewById(R.id.downVote);
            upVoteLabel = view.findViewById(R.id.upVoteLabel);
            downVoteLabel = view.findViewById(R.id.downVoteLabel);
            content = view.findViewById(R.id.content);
            comments = view.findViewById(R.id.comments);
            time = view.findViewById(R.id.time);
        }
    }
}