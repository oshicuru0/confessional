package app.or.confessional.settings;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.utils.ConfessionalShowcaseHelper;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.TextUtil;

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends Fragment {

    @ViewById
    public SwitchCompat newConfession;

    @ViewById
    public SwitchCompat voteOnConfession;

    @ViewById
    public SwitchCompat commentOnConfession, replyComment, voteOnReplyComment;

    @ViewById
    public SwitchCompat voteOnComment;

    @ViewById
    public View parent;

    public SettingsFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), newConfession);
        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), voteOnConfession);
        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), commentOnConfession);
        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), voteOnComment);
        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), replyComment);
        TextUtil.setFont(true, mainActivity.getString(R.string.AvenirNextBold), voteOnReplyComment);

        newConfession.setChecked(StorageHelper.getConfessionNotification(mainActivity));
        voteOnConfession.setChecked(StorageHelper.getVoteOnConfessionNotification(mainActivity));
        commentOnConfession.setChecked(StorageHelper.getCommentOnConfessionNotification(mainActivity));
        voteOnComment.setChecked(StorageHelper.getVoteOnCommentNotification(mainActivity));
        replyComment.setChecked(StorageHelper.getVoteOnReplyCommentNotification(mainActivity));
        voteOnReplyComment.setChecked(StorageHelper.getReplyCommentNotification(mainActivity));
    }

    @CheckedChange(resName = "newConfession")
    protected void newConfession() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveNewConfessionNotification(mainActivity, newConfession.isChecked());
    }

    @CheckedChange(resName = "voteOnConfession")
    protected void voteOnConfession() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveVoteOnConfessionNotification(mainActivity, voteOnConfession.isChecked());
    }

    @CheckedChange(resName = "commentOnConfession")
    protected void commentOnConfession() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveCommentOnConfessionNotification(mainActivity, commentOnConfession.isChecked());
    }

    @CheckedChange(resName = "voteOnComment")
    protected void voteOnComment() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveVoteOnCommentNotification(mainActivity, voteOnComment.isChecked());
    }

    @CheckedChange(resName = "replyComment")
    protected void replyComment() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveVoteOnReplyCommentNotification(mainActivity, replyComment.isChecked());
    }

    @CheckedChange(resName = "voteOnReplyComment")
    protected void voteOnReplyComment() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        StorageHelper.saveReplyCommentNotification(mainActivity, voteOnReplyComment.isChecked());
    }

    @Click(resName = "tooltip")
    protected void onTooltip() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessionalShowcaseHelper.resetAllShowCases();
        Snackbar.make(parent, "All tooltips shown. When you go to screen, tooltip will pops up!", Snackbar.LENGTH_LONG).show();
    }

    @Click(resName = "addInstagram")
    protected void addInstagram() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Uri uri = Uri.parse("https://www.instagram.com/confessional.us/?hl=en");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/confessional.us/?hl=en")));
        }
    }

    @Click(resName = "addTwitter")
    protected void addTwitter() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Uri uri = Uri.parse("https://twitter.com/ConfessionalU");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.twitter.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/ConfessionalU")));
        }
    }

    @Click(resName = "addFacebook")
    protected void addFacebook() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        try {
            mainActivity.getPackageManager().getPackageInfo("com.facebook.katana", 0);
             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/444644795943134")));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/confessional.us/")));
        }
    }
}