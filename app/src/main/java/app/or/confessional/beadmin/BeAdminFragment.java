package app.or.confessional.beadmin;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.ConfessionalShowcaseHelper;
import app.or.confessional.widget.ORDialogBeAdminInfo;

@EFragment(R.layout.fragment_be_admin)
public class BeAdminFragment extends Fragment implements BeAdminAdapter.OnChatItemClick{

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public ProgressBar progressLoader;

    @ViewById
    public View placeholder;

    private BeAdminAdapter beAdminAdapter;


    public BeAdminFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mainActivity.setMainButtonClick(new MainActivity.MainButtonClick() {
            @Override
            public void onSort() {
                ORDialogBeAdminInfo.createDialog(mainActivity).show();
            }
        });

        beAdminAdapter = new BeAdminAdapter();
        beAdminAdapter.setOnChatItemClick(this);
        recycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        recycler.addItemDecoration(new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL));
        recycler.setAdapter(beAdminAdapter);
        getFeeds();

        showForAdd();
    }

    @UiThread(delay = 300)
    public void showForAdd() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessionalShowcaseHelper.showInfo(mainActivity);
    }

    public void getFeeds() {
        progressLoader.setVisibility(View.VISIBLE);
        FeedHelper.getAllPendingFeeds(new FeedHelper.FeedHandler() {
            @Override
            public void onResponse(Object response) {
                if (response == null || !(response instanceof List)) {
                    return;
                }

                final MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
                    return;
                }

                if (((List) response).size() == 0) {
                    placeholder.setVisibility(View.VISIBLE);
                } else {
                    placeholder.setVisibility(View.GONE);
                }


                progressLoader.setVisibility(View.GONE);
                beAdminAdapter.setConfessions((List<Confession>) response);
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Intent intent = new Intent(mainActivity, CommentsActivity_.class);
        intent.putExtra("type", "BeAdmin");
        intent.putExtra("data", beAdminAdapter.getConfessions().get(position));
      //  mainActivity.startActivity(intent);
    }
}
