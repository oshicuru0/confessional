package app.or.confessional.mostpopular;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.utils.AdMobHelper;

@EFragment(R.layout.fragment_most_popular_frg)
public class MostPopularFragment extends Fragment implements MostPopularAdapter.OnChatItemClick{

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public ProgressBar progressLoader;

    @ViewById
    public AdView adView;

    private MostPopularAdapter mostPopularAdapter;
    public MostPopularFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mostPopularAdapter = new MostPopularAdapter();
        mostPopularAdapter.setOnChatItemClick(this);
        recycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        recycler.setAdapter(mostPopularAdapter);
        getFeeds();

        AdMobHelper.showAdd(adView);
    }

    @Override
    public void onItemClick(int position) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Intent intent = new Intent(mainActivity, CommentsActivity_.class);
        intent.putExtra("type", "MostPopular");
        intent.putExtra("confessionalId", mostPopularAdapter.getConfessions().get(position).getId());
        mainActivity.startActivity(intent);
    }

    public void getFeeds() {
        progressLoader.setVisibility(View.VISIBLE);
        FeedHelper.getAllMostPopular(new FeedHelper.FeedHandler() {
            @Override
            public void onResponse(Object response) {
                if (response == null || !(response instanceof List)) {
                    return;
                }

                final MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
                    return;
                }

                progressLoader.setVisibility(View.GONE);
                mostPopularAdapter.setConfessions((List<Confession>) response);
            }
        });
    }
}
