package app.or.confessional.mostpopular;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ConfessionalTextView;


public class MostPopularAdapter extends RecyclerView.Adapter<MostPopularAdapter.InboxViewHolder> implements View.OnClickListener {

    private List<Confession> confessions = new ArrayList<>();
    private OnChatItemClick onChatItemClick;

    public void addData(List<Confession> chatRooms) {
        this.confessions = chatRooms;
    }

    @Override
    public void onClick(View v) {

    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public View upVote, downVote;
        public ConfessionalTextView downVoteLabel, upVoteLabel, content, comments, time;

        public InboxViewHolder(View view) {
            super(view);
            itemView = view;
            upVote = view.findViewById(R.id.upVote);
            downVote = view.findViewById(R.id.downVote);
            upVoteLabel = view.findViewById(R.id.upVoteLabel);
            downVoteLabel = view.findViewById(R.id.downVoteLabel);
            content = view.findViewById(R.id.content);
            comments = view.findViewById(R.id.comments);
            time = view.findViewById(R.id.time);
        }
    }

    public MostPopularAdapter() {
        this.confessions = new ArrayList<>();
    }

    public void setConfessions(List<Confession> confessions) {
        this.confessions = confessions;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.most_popular_item, parent, false);
        return new InboxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final InboxViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Confession confession = confessions.get(position);

        holder.content.setText(confession.getConfession());
        holder.time.setText(formatDate(confession.getTimestamp()));
        holder.comments.setText(String.format("%s comments", String.valueOf(confession.getNumOfComments())));

        Integer [] data = confession.getConfessionInfo().get();

        holder.downVoteLabel.setText(String.valueOf(data[1]));
        holder.upVoteLabel.setText(String.valueOf(data[0]));

        Object value = confession.getConfessionInfo().getData().get(UserData.getInstance().getUser().getUserId());
        if (value != null) {
            if (Boolean.valueOf(String.valueOf(value))) {
                holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_disabled, 0, 0, 0);
                holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

                holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorAccent));
                holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
            } else {
                holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
                holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_disabled, 0, 0, 0);

                holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
                holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorAccent));
            }
        } else {
            holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
            holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

            holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
            holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
        }


        holder.upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedHelper.upVote(confession);
            }
        });

        holder.downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedHelper.downVote(confession);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onChatItemClick != null) {
                    onChatItemClick.onItemClick(position);
                }
            }
        });

        CommentHelper.getCommentCount(confession, new CommentHelper.CommentHandler() {
            @Override
            public void onResponse(Object response) {
                holder.comments.setText(String.format("%s comments", String.valueOf(response)));
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    private String formatDate(Long timestamp) {
        Long currentTime = System.currentTimeMillis();
        long resultTime = currentTime - timestamp;
        int hours = (int) ((resultTime / (1000 * 60 * 60)) % 24);
        int minutes = (int) ((resultTime / (1000 * 60)));
        int seconds = (int) ((resultTime / (1000)));
        int days = (int) ((resultTime / (1000 * 60 * 60 * 24)));

        if (days > 0) {
            return String.format("%sd ago", days);
        } else if (hours > 0) {
            return String.format("%sh ago", hours);
        } else if (minutes > 0) {
            return String.format("%sm ago", minutes);
        } else if (seconds > 0) {
            return String.format("%ss ago", seconds);
        }

        return String.format("%ss ago", seconds);
    }

    @Override
    public int getItemCount() {
        return confessions.size();
    }

    public void setOnChatItemClick(OnChatItemClick onChatItemClick) {
        this.onChatItemClick = onChatItemClick;
    }

    public List<Confession> getConfessions() {
        return confessions;
    }

    public interface OnChatItemClick {
        void onItemClick(int position);
    }
}