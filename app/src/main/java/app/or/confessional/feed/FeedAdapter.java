package app.or.confessional.feed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.AdMobHelper;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ConfessionalTextView;

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private OnChatItemClick onChatItemClick;
    private Activity activity;
    private ArrayList<Object> arrayList = new ArrayList<>();
    private static final int MENU_ITEM_VIEW_TYPE = 0;
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;

    @Override
    public void onClick(View v) {

    }

    public List<Object> getData() {
        return arrayList;
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public View upVote, downVote, share;
        public ConfessionalTextView downVoteLabel, upVoteLabel, content, comments, time;

        public InboxViewHolder(View view) {
            super(view);
            itemView = view;
            upVote = view.findViewById(R.id.upVote);
            share = view.findViewById(R.id.share);
            downVote = view.findViewById(R.id.downVote);
            upVoteLabel = view.findViewById(R.id.upVoteLabel);
            downVoteLabel = view.findViewById(R.id.downVoteLabel);
            content = view.findViewById(R.id.content);
            comments = view.findViewById(R.id.comments);
            time = view.findViewById(R.id.time);
        }
    }


    public class UnifiedNativeAdViewHolder extends RecyclerView.ViewHolder {

        private UnifiedNativeAdView adView;

        public UnifiedNativeAdView getAdView() {
            return adView;
        }

        UnifiedNativeAdViewHolder(View view) {
            super(view);
            adView = (UnifiedNativeAdView) view.findViewById(R.id.ad_view);
            adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));
            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_icon));
            adView.setPriceView(adView.findViewById(R.id.ad_price));
            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
            adView.setStoreView(adView.findViewById(R.id.ad_store));
            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        }
    }

    public class UnifiedNativeAdViewHolder1 extends RecyclerView.ViewHolder {

        public AdView adView, adView1, adView2;

        UnifiedNativeAdViewHolder1(View view) {
            super(view);
            adView = view.findViewById(R.id.adView);
            adView1 = view.findViewById(R.id.adView1);
            adView2 = view.findViewById(R.id.adView2);
        }
    }

    public FeedAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setConfessions(List<Confession> confessions) {
        arrayList.clear();
        arrayList.addAll(confessions);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                /*View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_undefined, parent, false);
                return new UnifiedNativeAdViewHolder(itemView);*/
                return new UnifiedNativeAdViewHolder1(LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_undefined1, parent, false));
            case MENU_ITEM_VIEW_TYPE:
                // Fall through.
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent, false);
                return new InboxViewHolder(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder h, @SuppressLint("RecyclerView") final int position) {
        if (h instanceof UnifiedNativeAdViewHolder1) {
           /* UnifiedNativeAd nativeAd = (UnifiedNativeAd) arrayList.get(position);
            populateNativeAdView(nativeAd, ((UnifiedNativeAdViewHolder) h).getAdView());*/
            UnifiedNativeAdViewHolder1 holder1 = (UnifiedNativeAdViewHolder1) h;
            AdMobHelper.showAdd(holder1.adView);
            //AdMobHelper.showAdd(holder1.adView1);
            //AdMobHelper.showAdd(holder1.adView2);
            return;
        }

        final InboxViewHolder holder = (InboxViewHolder) h;
        final Confession confession = (Confession) arrayList.get(position);

        holder.content.setText(confession.getConfession());
        holder.time.setText(formatDate(confession.getTimestamp()));
        holder.comments.setText(String.format("%s comments", String.valueOf(confession.getNumOfComments())));
        Integer[] data = confession.getConfessionInfo().get();

        holder.downVoteLabel.setText(String.valueOf(data[1]));
        holder.upVoteLabel.setText(String.valueOf(data[0]));

        Object value = confession.getConfessionInfo().getData().get(UserData.getInstance().getUser().getUserId());
        if (value != null) {
            if (Boolean.valueOf(String.valueOf(value))) {
                holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_disabled, 0, 0, 0);
                holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

                holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorAccent));
                holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
            } else {
                holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
                holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_disabled, 0, 0, 0);

                holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
                holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorAccent));
            }
        } else {
            holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
            holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

            holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
            holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
        }


        holder.upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedHelper.upVote(confession);
            }
        });

        holder.downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedHelper.downVote(confession);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onChatItemClick != null) {
                    onChatItemClick.onItemClick(position);
                }
            }
        });

        CommentHelper.getCommentCount(confession, new CommentHelper.CommentHandler() {
            @Override
            public void onResponse(Object response) {
                holder.comments.setText(String.format("%s comments", String.valueOf(response)));
            }

            @Override
            public void onError(String message) {

            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.shareConfession(confession, activity);
            }
        });
    }

    private void populateNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    private String formatDate(Long timestamp) {
        Long currentTime = System.currentTimeMillis();
        long resultTime = currentTime - timestamp;
        int hours = (int) ((resultTime / (1000 * 60 * 60)) % 24);
        int minutes = (int) ((resultTime / (1000 * 60)));
        int seconds = (int) ((resultTime / (1000)));
        int days = (int) ((resultTime / (1000 * 60 * 60 * 24)));

        if (days > 0) {
            return String.format("%sd ago", days);
        } else if (hours > 0) {
            return String.format("%sh ago", hours);
        } else if (minutes > 0) {
            return String.format("%sm ago", minutes);
        } else if (seconds > 0) {
            return String.format("%ss ago", seconds);
        }

        return String.format("%ss ago", seconds);
    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = getData().get(position);
        if (recyclerViewItem == null) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        }
        return MENU_ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public void setOnChatItemClick(OnChatItemClick onChatItemClick) {
        this.onChatItemClick = onChatItemClick;
    }

    public List<Object> getConfessions() {
        return arrayList;
    }

    public interface OnChatItemClick {
        void onItemClick(int position);
    }
}