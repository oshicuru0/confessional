package app.or.confessional.feed;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.MainActivity;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.AdMobHelper;
import app.or.confessional.utils.ConfessionalShowcaseHelper;
import app.or.confessional.widget.ConfessDialog;

@EFragment(R.layout.fragment_feed)
public class FeedFragment extends Fragment implements FeedAdapter.OnChatItemClick {

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public FloatingActionButton confess;

    @ViewById
    public ProgressBar progressLoader;

    @ViewById
    public View root;

    @ViewById
    public AdView adView;

    private FeedAdapter feedAdapter;

    private InterstitialAd interstitialAd;

    public FeedFragment() {
    }

    public static final int NUMBER_OF_ADS = 2;
    private AdLoader adLoader;
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        feedAdapter = new FeedAdapter(mainActivity);
        feedAdapter.setOnChatItemClick(this);
        recycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        recycler.addItemDecoration(new DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL));
        recycler.setAdapter(feedAdapter);
        getFeeds();
        mainActivity.setMainButtonClick(new MainActivity.MainButtonClick() {
            @Override
            public void onSort() {
                onConfess();
            }
        });

        showForAdd();

        //AdMobHelper.showAdd(adView);

        interstitialAd = AdMobHelper.showAdd(mainActivity);
    }

    private void insertAdsInMenuItems() {
        int offset = (feedAdapter.getItemCount() / 2) + 1;
        int index = 0;
        for (int i = 0; i < 2; i++) {
            feedAdapter.getData().add(index, null);
            index = index + offset;
        }
        /*
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (feedAdapter.getItemCount() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad: mNativeAds) {
            feedAdapter.getData().add(index, ad);
            index = index + offset;
        }

        feedAdapter.notifyDataSetChanged();
        */
    }

    private void loadNativeAds() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        insertAdsInMenuItems();
        /*
        AdLoader.Builder builder = new AdLoader.Builder(mainActivity, getString(R.string.unit_3));
        adLoader = builder.forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        mNativeAds.add(unifiedNativeAd);
                        if (!adLoader.isLoading()) {
                            insertAdsInMenuItems();
                        }
                    }
                }).withAdListener(
                new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        if (!adLoader.isLoading()) {
                            insertAdsInMenuItems();
                        }
                    }
                }).build();
        adLoader.loadAds(new AdRequest.Builder().build(), NUMBER_OF_ADS);*/
    }

    @Click(resName = "confess")
    protected void onConfess() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessDialog.createDialog(mainActivity).setListener(new ConfessDialog.ButtonClick() {
            @Override
            public void onYes(String message) {
                Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
                if (interstitialAd != null) {
                    interstitialAd.show();
                }
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

    @Override
    public void onItemClick(int position) {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        Intent intent = new Intent(mainActivity, CommentsActivity_.class);
        intent.putExtra("type", "Feed");
        intent.putExtra("confessionalId", ((Confession) feedAdapter.getConfessions().get(position)).getId());
        mainActivity.startActivity(intent);
    }

    public void getFeeds() {
        progressLoader.setVisibility(View.VISIBLE);
        FeedHelper.getAllFeeds(new FeedHelper.FeedHandler() {
            @Override
            public void onResponse(Object response) {
                if (response == null || !(response instanceof List)) {
                    return;
                }

                final MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
                    return;
                }

                progressLoader.setVisibility(View.GONE);
                feedAdapter.setConfessions((List<Confession>) response);
                loadNativeAds();
            }
        });
    }

    @UiThread(delay = 120)
    public void showForAdd() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        ConfessionalShowcaseHelper.showAddNewConfessions(mainActivity);
    }
}
