package app.or.confessional.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import app.or.confessional.R;
import app.or.confessional.widget.ConfessionalTextView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by osman on 5/12/2018.
 */

public class ConfessionalShowcaseHelper {
    public static void showAddNewConfessions(Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1000")
                        .on(R.id.sort)
                        .addCircle()
                        .withBorder()
                        .show();

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("Tap on this icon opens dialog for adding new confession!");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Add new confession");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_add);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void showComment(final Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1001")
                        .on(R.id.comments)
                        .addCircle()
                        .withBorder()
                        .show();

        showcase.setListener(new ConfessionalShowcase.Listener() {
            @Override
            public void onDismissed() {
                showUpVote(activity);
            }
        });

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Add new comment");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_comment);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void showUpVote(final Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1002")
                        .on(R.id.upVote)
                        .addCircle()
                        .withBorder()
                        .show();

        showcase.setListener(new ConfessionalShowcase.Listener() {
            @Override
            public void onDismissed() {
                showDownVote(activity);
            }
        });

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Up vote");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_thumb_up_sign);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void showDownVote(Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1003")
                        .on(R.id.downVote)
                        .addCircle()
                        .withBorder()
                        .show();

        showcase.setListener(new ConfessionalShowcase.Listener() {
            @Override
            public void onDismissed() {

            }
        });

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Down vote");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_thumb_down_sign);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void showDownload(final Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1004")
                        .on(R.id.download)
                        .addCircle()
                        .withBorder()
                        .show();

        showcase.setListener(new ConfessionalShowcase.Listener() {
            @Override
            public void onDismissed() {
                showComment(activity);
            }
        });

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Download confession");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_download);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void showInfo(final Activity activity) {
        View view = LayoutInflater.from(activity).inflate(R.layout.f_bomb_showcase, null);

        final ConfessionalShowcase showcase =
                ConfessionalShowcase.from(activity)
                        .setContentView(view)
                        .setId("1005")
                        .on(R.id.sort)
                        .addCircle()
                        .withBorder()
                        .show();

        showcase.setListener(new ConfessionalShowcase.Listener() {
            @Override
            public void onDismissed() {
            }
        });

        ((ConfessionalTextView) view.findViewById(R.id.message)).setText("Tap on this icon will open info what is purpose of Be admin section");
        ((ConfessionalTextView) view.findViewById(R.id.title)).setText("Info");
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_info);

        view.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showcase.doNotShowAgain();
                showcase.dismiss();
            }
        });
    }

    public static void resetAllShowCases() {
        SharedPreferences sharedPreference = UserData.getInstance().getActivity().getSharedPreferences("ConfessionalShowcase", MODE_PRIVATE);
        sharedPreference.edit().clear().apply();
    }
}
