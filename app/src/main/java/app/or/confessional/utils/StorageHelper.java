
package app.or.confessional.utils;

import android.content.Context;
import android.content.SharedPreferences;

import app.or.confessional.activities.LoginHelper;
import app.or.confessional.resources.model.User;

/**
 * Created by RESIDOVI on 4/27/2018.
 */

public class StorageHelper {
    private static final String USER_DATA = "user_data";
    private static final String USER_DATA_USER_ID = "userId";
    private static final String NEW_CONFESSION_NOTIFICATION = "new_confession_notification";
    private static final String VOTE_ON_CONFESSION_NOTIFICATION = "vote_on_confession_notification";
    private static final String REPLY_COMMENT_ON_CONFESSION_NOTIFICATION = "comment_on_confession_notification";
    private static final String COMMENT_ON_CONFESSION_NOTIFICATION = "reply_comment_on_confession_notification";
    private static final String VOTE_ON_COMMENT_NOTIFICATION = "vote_on_comment_notification";
    private static final String VOTE_ON_REPLY_COMMENT_NOTIFICATION = "vote_on_reply_comment_notification";

    public static void initializeUserData(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        User user = UserData.getInstance().getUser();
        user.setUserId(data.getString(USER_DATA_USER_ID,  null));
        if (user.getUserId() != null) {
            user.setUserType("user");
        }
    }

    public static void refreshUserData(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        User user = UserData.getInstance().getUser();
        user.setUserType("user");
        data.edit().putString(USER_DATA_USER_ID, user.getUserId()).apply();
    }

    public static void intGuestUser(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        if (data.getString(USER_DATA_USER_ID,  null) == null) {
            UserData.getInstance().getUser().setUserId(Tools.getUniqueIdentification(context));
        } else {
            initializeUserData(context);
        }
    }

    public static void deleteUserData(Context context) {
        LoginHelper.removeNotificationToken();
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().clear().apply();
        UserData.getInstance().setUser(new User());
    }

    public static boolean getConfessionNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(NEW_CONFESSION_NOTIFICATION, true);
    }

    public static boolean getVoteOnConfessionNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(VOTE_ON_CONFESSION_NOTIFICATION, true);
    }

    public static boolean getCommentOnConfessionNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(COMMENT_ON_CONFESSION_NOTIFICATION, true);
    }

    public static boolean getVoteOnCommentNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(VOTE_ON_COMMENT_NOTIFICATION, true);
    }

    public static boolean getReplyCommentNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(REPLY_COMMENT_ON_CONFESSION_NOTIFICATION, true);
    }

    public static boolean getVoteOnReplyCommentNotification(Context context) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return data.getBoolean(VOTE_ON_REPLY_COMMENT_NOTIFICATION, true);
    }

    public static void saveNewConfessionNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(NEW_CONFESSION_NOTIFICATION, state).apply();
    }

    public static void saveVoteOnConfessionNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(VOTE_ON_CONFESSION_NOTIFICATION, state).apply();
    }

    public static void saveCommentOnConfessionNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(COMMENT_ON_CONFESSION_NOTIFICATION, state).apply();
    }

    public static void saveVoteOnCommentNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(VOTE_ON_COMMENT_NOTIFICATION, state).apply();
    }

    public static void saveVoteOnReplyCommentNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(VOTE_ON_REPLY_COMMENT_NOTIFICATION, state).apply();
    }

    public static void saveReplyCommentNotification(Context context, boolean state) {
        SharedPreferences data = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        data.edit().putBoolean(REPLY_COMMENT_ON_CONFESSION_NOTIFICATION, state).apply();
    }
}
