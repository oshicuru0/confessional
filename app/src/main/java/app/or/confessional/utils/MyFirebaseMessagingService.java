package app.or.confessional.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.util.Random;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity_;
import app.or.confessional.activities.ReplaysActivity_;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            sendNotification(remoteMessage);
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) throws IOException {
        String contentText = remoteMessage.getData().get("content");
        String type = remoteMessage.getData().get("type");

        if ("vote".equals(type) && !StorageHelper.getVoteOnConfessionNotification(this)) {
            return;
        }

        if ("comment".equals(type) && !StorageHelper.getCommentOnConfessionNotification(this)) {
            return;
        }

        if ("voteOnComment".equals(type) && !StorageHelper.getVoteOnCommentNotification(this)) {
            return;
        }

        if ("voteOnReplyComment".equals(type) && !StorageHelper.getVoteOnReplyCommentNotification(this)) {
            return;
        }

        if ("replyComment".equals(type) && !StorageHelper.getReplyCommentNotification(this)) {
            return;
        }

        String confessionId = remoteMessage.getData().get("confessionId");
        String commentId = remoteMessage.getData().get("commentId");
        String replyCommentId = remoteMessage.getData().get("replyCommentId");

        Intent intent = null;
        if ("voteOnReplyComment".equals(type) || "replyComment".equals(type)) {
            intent = new Intent(this, ReplaysActivity_.class);
            intent.putExtra("fromNotification", true);
        } else {
            intent = new Intent(this, CommentsActivity_.class);
        }

        intent.putExtra("confessionalId", confessionId);
        intent.putExtra("commentId", commentId);
        intent.putExtra("replyCommentId", replyCommentId);
        intent.putExtra("type", "Feed");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "799")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Anonymous")
                .setContentText(contentText)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("799", "channel_003", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(new Random().nextInt(100), notificationBuilder.build());
        }
    }
}