package app.or.confessional.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import app.or.confessional.R;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.widget.ConfessionalTextView;

/**
 * Created by RESIDOVI on 4/27/2018.
 */

public class BitmapHelper {
    public static void createBitmap(final Confession confession, final Activity context, final DownloadConfession downloadConfession, final int color) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = createBitmapFromLayoutWithText(context, confession, color);

                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pictures/Confessional";
                new File(file_path).mkdirs();
                String filename = confession.getId();
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(file_path + "/" + filename + ".png");
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "", "");

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (downloadConfession != null) {
                        downloadConfession.onCompleted(file_path + "/" + filename + ".png");
                    }
                }
            }
        }).start();
    }

    public static Bitmap createBitmapFromLayoutWithText(Activity context, Confession confession, int color) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Inflate the layout into a view and configure it the way you like
        RelativeLayout view = new RelativeLayout(context);
        mInflater.inflate(R.layout.template_confession_layout, view, true);
        view.findViewById(R.id.layer).setBackgroundColor(ContextCompat.getColor(context, color));
        ConfessionalTextView tv = view.findViewById(R.id.content);
        tv.setText(confession.getConfession());

        //Provide it with a layout params. It should necessarily be wrapping the
        //content as we not really going to have a parent for it.
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

        //Pre-measure the view so that height and width don't remain null.
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        //Assign a size and position to the view and all of its descendants
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);

        //Render this view (and all of its children) to the given Canvas
        view.draw(c);
        return bitmap;
    }

    public interface DownloadConfession {
        void onCompleted(String uri);
    }
}
