package app.or.confessional.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class ConfessionalShowcase {

    public interface Listener {
        void onDismissed();
    }

    public static final float DEFAULT_ADDITIONAL_RADIUS_RATIO = 1.5f;
    private FrameLayout container;
    private ConfessionalShowcaseView ConfessionalShowcaseView;
    private Listener listener;
    private String id;

    private ConfessionalShowcase(@NonNull Activity activity) {
        this.container = new FrameLayout(activity);
        this.ConfessionalShowcaseView = new ConfessionalShowcaseView(activity);
        Window window = activity.getWindow();
        if (window != null) {
            ViewGroup decorView = (ViewGroup) window.getDecorView();
            if (decorView != null) {
                ViewGroup content = decorView.findViewById(android.R.id.content);
                if (content != null) {
                    content.addView(container, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    this.container.addView(ConfessionalShowcaseView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                }
            }
        }
        this.container.setVisibility(View.GONE);
        ViewCompat.setAlpha(container, 0f);
    }

    @NonNull
    public static ConfessionalShowcase from(@NonNull Activity activity) {
        return new ConfessionalShowcase(activity);
    }

    public ConfessionalShowcase setBackgroundColor(@ColorInt int color) {
        ConfessionalShowcaseView.setBackgroundOverlayColor(color);
        return this;
    }

    public ConfessionalShowcase setListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    public ConfessionalShowcase setContentView(View view) {
        container.addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return this;
    }

    public ConfessionalShowcase setId(String id) {
        this.id = id;
        return this;
    }

    public String getId() {
        return id;
    }

    public void doNotShowAgain() {
        SharedPreferences sharedPreference = container.getContext().getSharedPreferences("ConfessionalShowcase", MODE_PRIVATE);
        if (sharedPreference != null) {
            sharedPreference.edit().putBoolean(getId(), false).commit();
        }
    }

    public void dismiss() {
        ViewCompat.animate(container)
                .alpha(0f)
                .setDuration(container.getResources().getInteger(android.R.integer.config_mediumAnimTime))
                .setListener(new ViewPropertyAnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(View view) {
                        super.onAnimationEnd(view);
                        ViewParent parent = view.getParent();
                        if (parent instanceof ViewGroup) {
                            ((ViewGroup) parent).removeView(view);
                        }
                        if (listener != null) {
                            listener.onDismissed();
                        }
                    }
                }).start();

    }

    public ConfessionalShowcase onClickContentView(int id, View.OnClickListener onClickListener) {
        View view = container.findViewById(id);
        if (view != null) {
            view.setOnClickListener(onClickListener);
        }
        return this;
    }

    public ConfessionalShowcase show() {
        SharedPreferences sharedPreference = container.getContext().getSharedPreferences("ConfessionalShowcase", MODE_PRIVATE);
        if (sharedPreference.getBoolean(String.valueOf(getId()), true)) {
            container.setVisibility(View.VISIBLE);
            ViewCompat.animate(container)
                    .alpha(1f)
                    .setDuration(container.getResources().getInteger(android.R.integer.config_longAnimTime))
                    .start();
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
        return this;
    }

    public ConfessionalShowcase showOnce(String key) {
        show();
        return this;
    }

    @Nullable
    private View findViewById(@IdRes int viewId) {
        Context context = ConfessionalShowcaseView.getContext();
        View view = null;
        if (context instanceof Activity) {
            view = ((Activity) context).findViewById(viewId);
        }
        return view;

    }

    public ViewActions on(@IdRes int viewId) {
        return new ViewActions(this, findViewById(viewId));
    }

    public ViewActions on(View view) {
        return new ViewActions(this, view);
    }

    private static class ViewActionsSettings {
        private boolean animated = true;
        private boolean withBorder = false;
        @Nullable
        private View.OnClickListener onClickListener;
    }

    public static class ViewActions {
        private final ConfessionalShowcase ConfessionalShowcase;
        private final View view;
        private final ViewActionsSettings settings;

        public ViewActions(final ConfessionalShowcase ConfessionalShowcase, View view) {
            this.ConfessionalShowcase = ConfessionalShowcase;
            this.view = view;
            this.settings = new ViewActionsSettings();
        }

        public ViewActions on(@IdRes int viewId) {
            return ConfessionalShowcase.on(viewId);
        }

        public ViewActions on(View view) {
            return ConfessionalShowcase.on(view);
        }

        public ConfessionalShowcase show() {
            return ConfessionalShowcase.show();
        }

        private void addCircleOnView(float additionalRadiusRatio) {
            Rect rect = new Rect();
            view.getGlobalVisibleRect(rect);

            int cx = rect.centerX();
            int cy = rect.centerY() - getStatusBarOffset();
            int radius = (int) (Math.max(rect.width(), rect.height()) / 2f * additionalRadiusRatio);
            Circle circle = new Circle(cx, cy, radius);
            circle.setDisplayBorder(settings.withBorder);
            ConfessionalShowcase.ConfessionalShowcaseView.addCircle(circle);

            addClickableView(rect, settings.onClickListener, additionalRadiusRatio);

            ConfessionalShowcase.ConfessionalShowcaseView.postInvalidate();
        }

        public ShapeViewActionsEditor addRoundRect() {
            return addRoundRect(DEFAULT_ADDITIONAL_RADIUS_RATIO);
        }

        public ShapeViewActionsEditor addRoundRect(final float additionalRadiusRatio) {
            view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    addRoundRectOnView(additionalRadiusRatio);
                    view.getViewTreeObserver().removeOnPreDrawListener(this);
                    return false;
                }
            });
            return new ShapeViewActionsEditor(this);
        }

        public ShapeViewActionsEditor addCircle() {
            return addCircle(DEFAULT_ADDITIONAL_RADIUS_RATIO);
        }

        public ShapeViewActionsEditor addCircle(final float additionalRadiusRatio) {
            if (view != null) {
                view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        addCircleOnView(additionalRadiusRatio);
                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                        return false;
                    }
                });

            }

            return new ShapeViewActionsEditor(this);
        }

        private void addRoundRectOnView(float additionalRadiusRatio) {
            Rect rect = new Rect();
            view.getGlobalVisibleRect(rect);

            int padding = 40;

            final int x = rect.left - padding;
            final int y = rect.top - getStatusBarOffset() - padding;
            final int width = rect.width() + 2 * padding;
            final int height = rect.height() + 2 * padding;

            RoundRect roundRect = new RoundRect(x, y, width, height);
            roundRect.setDisplayBorder(settings.withBorder);
            ConfessionalShowcase.ConfessionalShowcaseView.addRoundRect(roundRect);
            addClickableView(rect, settings.onClickListener, additionalRadiusRatio);
            ConfessionalShowcase.ConfessionalShowcaseView.postInvalidate();
        }

        private int getStatusBarOffset() {
            int result = 0;
            Context context = view.getContext();
            Resources resources = context.getResources();
            int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId);
            }

            return result;
        }

        private void addClickableView(Rect rect, View.OnClickListener onClickListener, float additionalRadiusRatio) {
            View cliclableView = new View(this.view.getContext());
            int width = (int) (rect.width() * additionalRadiusRatio);
            int height = (int) (rect.height() * additionalRadiusRatio);
            int x = rect.left - (width - rect.width()) / 2;
            int y = rect.top - (height - rect.height()) / 2 - getStatusBarOffset();
            cliclableView.setLayoutParams(new ViewGroup.MarginLayoutParams(width, height));
            ViewCompat.setTranslationY(cliclableView, y);
            ViewCompat.setTranslationX(cliclableView, x);
            cliclableView.setOnClickListener(onClickListener);
            ConfessionalShowcase.container.addView(cliclableView);
            ConfessionalShowcase.container.invalidate();
        }

        public ConfessionalShowcase showOnce(String key) {
            return ConfessionalShowcase.showOnce(key);
        }

        public ConfessionalShowcase onClickContentView(@IdRes int viewId, View.OnClickListener onClickListener) {
            return ConfessionalShowcase.onClickContentView(viewId, onClickListener);
        }
    }

    public static class ViewActionsEditor {
        protected final ViewActions viewActions;

        public ViewActionsEditor(ViewActions viewActions) {
            this.viewActions = viewActions;
        }

        public ViewActions on(@IdRes int viewId) {
            return viewActions.on(viewId);
        }

        public ViewActions on(View view) {
            return viewActions.on(view);
        }

        public ConfessionalShowcase show() {
            return viewActions.show();
        }

        public ConfessionalShowcase showOnce(String key) {
            return viewActions.showOnce(key);
        }

        public ConfessionalShowcase onClickContentView(@IdRes int viewId, View.OnClickListener onClickListener) {
            return viewActions.onClickContentView(viewId, onClickListener);
        }
    }

    public static class ShapeViewActionsEditor extends ViewActionsEditor {
        public ShapeViewActionsEditor(ViewActions viewActions) {
            super(viewActions);
        }

        public ShapeViewActionsEditor withBorder() {
            this.viewActions.settings.withBorder = true;
            return this;
        }

        public ShapeViewActionsEditor onClick(View.OnClickListener onClickListener) {
            this.viewActions.settings.onClickListener = onClickListener;
            return this;
        }
    }

    public static class Circle extends Shape {
        private int x;
        private int y;
        private int radius;

        public Circle(int x, int y, int radius) {
            super();
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getRadius() {
            return radius;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }

        @Override
        public void drawOn(Canvas canvas) {
            if (isDisplayBorder()) {
                canvas.drawCircle(getX(), getY(), getRadius() * 1.2f, getBorderPaint());
            }

            canvas.drawCircle(getX(), getY(), getRadius(), getPaint());
        }
    }

    public abstract static class Shape {
        private int color = Color.argb(0, 0, 0, 0);
        protected Paint paint;

        private int borderColor = Color.parseColor("#AA999999");
        private Paint borderPaint;

        private boolean displayBorder = false;

        public Shape() {
            this.paint = new Paint();
            this.paint.setColor(getColor());
            this.paint.setAntiAlias(true);
            this.paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));

            this.borderPaint = new Paint();
            this.borderPaint.setAntiAlias(true);
            this.borderPaint.setColor(borderColor);
        }

        public void setColor(int color) {
            this.color = color;
            this.paint.setColor(this.color);
        }

        public int getColor() {
            return color;
        }

        public Paint getPaint() {
            return paint;
        }

        public int getBorderColor() {
            return borderColor;
        }

        public boolean isDisplayBorder() {
            return displayBorder;
        }

        public void setDisplayBorder(boolean displayBorder) {
            this.displayBorder = displayBorder;
        }

        public void setBorderColor(int borderColor) {
            this.borderColor = borderColor;
            this.paint.setColor(borderColor);
        }

        public Paint getBorderPaint() {
            return borderPaint;
        }

        public abstract void drawOn(Canvas canvas);
    }

    static class ConfessionalShowcaseView extends View {

        static final int DEFAULT_ALPHA_COLOR = 220;
        int backgroundOverlayColor = Color.argb(DEFAULT_ALPHA_COLOR, 0, 0, 0);
        List<Shape> shapes;

        ConfessionalShowcaseView(Context context) {
            super(context);
            initialize();
        }

        ConfessionalShowcaseView(Context context, AttributeSet attrs) {
            super(context, attrs);
            initialize();
        }

        ConfessionalShowcaseView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            initialize();
        }

        public void addCircle(Circle circle) {
            this.shapes.add(circle);
        }

        public void addRoundRect(RoundRect roundRect) {
            this.shapes.add(roundRect);
        }

        public int getBackgroundOverlayColor() {
            return backgroundOverlayColor;
        }

        public void setBackgroundOverlayColor(int backgroundOverlayColor) {
            this.backgroundOverlayColor = backgroundOverlayColor;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawColor(backgroundOverlayColor);
            for (Shape shape : shapes) {
                shape.drawOn(canvas);
            }

        }

        private void initialize() {
            shapes = new ArrayList<>();

            setDrawingCacheEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

        }
    }

    public static class RoundRect extends Shape {

        private int x;
        private int y;
        private int width;
        private int height;
        public static final int BORDER_PADDING = 30;

        public RoundRect(int x, int y, int width, int height) {
            super();
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        @Override
        public void drawOn(Canvas canvas) {
            if (isDisplayBorder()) {
                drawRoundedRect(canvas, getX() - BORDER_PADDING, getY() - BORDER_PADDING, getX() + getWidth() + BORDER_PADDING, getY() + getHeight() + BORDER_PADDING, getBorderPaint());
            }
            drawRoundedRect(canvas, getX(), getY(), getX() + getWidth(), getY() + getHeight(), paint);
        }

        private static void drawRoundedRect(Canvas canvas, float left, float top, float right, float bottom, Paint paint) {
            float radius = (bottom - top) / 2;

            RectF rectF = new RectF(left, top, right, bottom);
            canvas.drawRoundRect(rectF, radius, radius, paint);
        }
    }
}
