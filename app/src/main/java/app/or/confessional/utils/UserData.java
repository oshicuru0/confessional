package app.or.confessional.utils;

import app.or.confessional.activities.MainActivity;
import app.or.confessional.resources.model.User;

/**
 * Created by RESIDOVI on 4/26/2018.
 */

public class UserData {
    private static UserData instance;
    public static UserData getInstance() {
        if (instance == null) {
            instance = new UserData();
        }

        return instance;
    }

    private boolean isMainRun = false;

    private User user = new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setMainRun(boolean mainRun) {
        isMainRun = mainRun;
    }

    public boolean isMainRun() {
        return isMainRun;
    }

    private MainActivity activity;

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public MainActivity getActivity() {
        return activity;
    }
}
