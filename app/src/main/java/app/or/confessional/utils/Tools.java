package app.or.confessional.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.DisplayMetrics;

import java.net.InetAddress;

import app.or.confessional.R;
import app.or.confessional.activities.CommentsActivity;
import app.or.confessional.resources.model.Confession;

/**
 * Created by osman on 4/21/2018.
 */

public class Tools {
    public static int dpToPx(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int) (dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @SuppressLint("HardwareIds")
    public static String getUniqueIdentification(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void shareConfession(Confession confession, final Activity activity) {
        BitmapHelper.createBitmap(confession, activity, new BitmapHelper.DownloadConfession() {
            @Override
            public void onCompleted(String uri) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                Uri imageUri =  Uri.parse(uri);
                share.putExtra(Intent.EXTRA_STREAM, imageUri);
                activity.startActivity(Intent.createChooser(share, "Select"));
            }
        }, R.color.colorPrimary);
    }

    public static void isInternetAvailable(Activity activity, final CheckInternet handler) {
        ConnectivityManager conMgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr == null) {
            handler.onFinished(false);
            return;
        }

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            handler.onFinished(true);
        } else {
            handler.onFinished(false);
        }
    }

    public static int getTextWidth(String text, Paint textPaint) {
        Rect bounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length() - 1, bounds);
        return bounds.width();
    }

    public interface CheckInternet {
        void onFinished(Boolean state);
    }
}
