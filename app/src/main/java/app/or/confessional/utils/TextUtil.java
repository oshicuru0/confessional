package app.or.confessional.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuItem;

/**
 * Created by Osman Residovic on 26/01/2018.
 */

public class TextUtil {
    public static void setFont(boolean setAllCaps, String font, SwitchCompat switchCompat) {
        AssetManager assets = switchCompat.getContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(assets, font);
        switchCompat.setTypeface(typeface);
        switchCompat.setAllCaps(setAllCaps);
    }

    public static void setFont(String font, TextInputLayout layout) {
        AssetManager assets = layout.getContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(assets, font);
        layout.setTypeface(typeface);
    }

    public static void setFont(Context context, String font, MenuItem menuItem) {
        AssetManager assets = context.getAssets();
        Typeface typeface = Typeface.createFromAsset(assets, font);
    }

}
