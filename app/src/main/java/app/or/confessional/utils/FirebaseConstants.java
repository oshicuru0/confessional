package app.or.confessional.utils;

/**
 * Created by RESIDOVI on 4/25/2018.
 */

public class FirebaseConstants {
    public static final String CONFESSIONS = "confessionals";
    public static final String CONFESSIONS_UP_VOTE = "numOfUpVote";
    public static final String CONFESSIONS_DOWN_VOTE = "numOfDownVote";
    public static final String CONFESSIONS_VIEW = "numOfViews";
    public static final String CONFESSIONS_OWNER_ID = "ownerId";
    public static final String CONFESSIONS_TIMESTAMP = "timestamp";

    public static final String COMMENTS = "comments";
    public static final String REPLAY_COMMENTS = "replayComments";
    public static final String COMMENTS_DOWN_VOTE = "downVotes";
    public static final String COMMENTS_UP_VOTE = "upVotes";

    public static final String USERS = "users";
    public static final String USERS_ID = "userId";
    public static final String USERS_PASSWORD = "password";

    public static final String CONFESSION_INFO = "confession_info";
    public static final String COMMENT_INFO = "comment_info";
}
