package app.or.confessional.widget;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import app.or.confessional.R;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.UserData;

public class CommentDialog extends BaseDialog<CommentDialog> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.95f;
    private ButtonClick listener;
    private View root;
    private Button comment, cancel;
    private EditText content;
    private Confession confession;
    private ConfessionalTextView charsCount;


    public CommentDialog(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public CommentDialog(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    public CommentDialog(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.add_new_comment, null);

        root = inflate.findViewById(R.id.root);
        root.setOnClickListener(this);

        cancel = inflate.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        comment = inflate.findViewById(R.id.comment);
        comment.setOnClickListener(this);

        content = inflate.findViewById(R.id.content);
        content.requestFocus();

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        charsCount = inflate.findViewById(R.id.chars_count);
        content.addTextChangedListener(countWatcher);
    }

    public static CommentDialog createDialog(Activity activity) {
        return new CommentDialog(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        if (v == comment) {
            if (content.getText().length() < 5) {
                content.setError("Comment mush have at least 5 characters!");
                return;
            }

            Comment comment = new Comment();
            comment.setContent(content.getText().toString());
            comment.setTimestamp(System.currentTimeMillis());
            comment.setOwnerId(UserData.getInstance().getUser().getUserId());
            comment.setConfessionalId(confession.getId());
            final ORProgressDialog progressDialog = new ORProgressDialog(context);
            progressDialog.show();
            CommentHelper.postComment(confession, comment, new CommentHelper.CommentHandler() {
                @Override
                public void onResponse(Object response) {
                    progressDialog.dismiss();
                    dismiss();
                    if (listener != null) {
                        listener.onYes(null);
                    }
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    dismiss();
                    if (listener != null) {
                        listener.onYes(message);
                    }
                }
            });
        }

        if (v == cancel) {
            dismiss();
        }
    }

    private TextWatcher countWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            charsCount.setText(String.format("%s/400", content.getText().length()));
        }
    };

    public CommentDialog setData(Confession confession) {
        this.confession = confession;
        return this;
    }

    public CommentDialog setListener(ButtonClick listener) {
        this.listener = listener;
        return this;
    }

    public interface ButtonClick {
        void onYes(String message);
        void onCancel();
    }
}