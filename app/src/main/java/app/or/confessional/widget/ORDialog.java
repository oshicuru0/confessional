package app.or.confessional.widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import app.or.confessional.R;

public class ORDialog extends BaseDialog<ORDialog> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.8f;
    private ButtonClick listener;

    public ORDialog(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public ORDialog(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    public ORDialog(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.dialog_layout, null);
    }

    public static ORDialog createDialog(Activity activity) {
        return new ORDialog(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {

    }

    public interface ButtonClick {
        void onYes();
        void onCancel();
    }
}