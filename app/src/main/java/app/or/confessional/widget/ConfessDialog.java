package app.or.confessional.widget;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import app.or.confessional.R;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.utils.UserData;

public class ConfessDialog extends BaseDialog<ConfessDialog> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.95f;
    private ButtonClick listener;
    private View root;
    private EditText content, tags;
    private Button confess, cancel;
    private ConfessionalTextView charsCount;

    public ConfessDialog(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public ConfessDialog(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    public ConfessDialog(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.add_new_confess, null);

        root = inflate.findViewById(R.id.root);
        root.setOnClickListener(this);

        confess = inflate.findViewById(R.id.confess);
        confess.setOnClickListener(this);

        cancel = inflate.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        content = inflate.findViewById(R.id.content);
        content.addTextChangedListener(countWatcher);
        content.requestFocus();

        tags = inflate.findViewById(R.id.tags);

        charsCount = inflate.findViewById(R.id.chars_count);

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public static ConfessDialog createDialog(Activity activity) {
        return new ConfessDialog(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        if (v == confess) {
            if (content.getText().length() < 15) {
                content.setError("Confession mush have at least 15 characters!");
                return;
            }

            Confession confession = new Confession();
            confession.setConfession(content.getText().toString());
            confession.setTimestamp(System.currentTimeMillis());
            confession.setOwnerId(UserData.getInstance().getUser().getUserId());
            final ORProgressDialog progressDIalog = new ORProgressDialog(context);
            progressDIalog.show();
            FeedHelper.postConfessional(confession, new FeedHelper.FeedHandler() {
                @Override
                public void onResponse(Object response) {
                    dismiss();
                    progressDIalog.dismiss();
                    listener.onYes(String.valueOf(response));
                }
            });
        }

        if (v == cancel) {
            dismiss();
        }
    }

    private TextWatcher countWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
           charsCount.setText(String.format("%s/400", content.getText().length()));
        }
    };

    public ConfessDialog setListener(ButtonClick listener) {
        this.listener = listener;
       return this;
    }

    public interface ButtonClick {
        void onYes(String message);
        void onCancel();
    }
}