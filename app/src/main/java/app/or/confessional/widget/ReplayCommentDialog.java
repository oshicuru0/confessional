package app.or.confessional.widget;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.utils.UserData;

public class ReplayCommentDialog extends BaseDialog<ReplayCommentDialog> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.95f;
    private ButtonClick listener;
    private View root;
    private Button comment, cancel;
    private EditText content;
    private String commentId, confessionalId;
    private ConfessionalTextView charsCount;


    public ReplayCommentDialog(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public ReplayCommentDialog(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    public ReplayCommentDialog(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.add_new_comment, null);

        root = inflate.findViewById(R.id.root);
        root.setOnClickListener(this);

        cancel = inflate.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        comment = inflate.findViewById(R.id.comment);
        comment.setOnClickListener(this);

        content = inflate.findViewById(R.id.content);
        content.requestFocus();

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        charsCount = inflate.findViewById(R.id.chars_count);
        content.addTextChangedListener(countWatcher);
    }

    public static ReplayCommentDialog createDialog(Activity activity) {
        return new ReplayCommentDialog(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        if (v == comment) {
            if (content.getText().length() < 5) {
                content.setError("Comment mush have at least 5 characters!");
                return;
            }

            Comment comment = new Comment();
            comment.setContent(content.getText().toString());
            comment.setTimestamp(System.currentTimeMillis());
            comment.setOwnerId(UserData.getInstance().getUser().getUserId());
            comment.setConfessionalId(confessionalId);
            final ORProgressDialog progressDialog = new ORProgressDialog(context);
            progressDialog.show();
            CommentHelper.postReplayComment(confessionalId, commentId, comment, new CommentHelper.CommentHandler() {
                @Override
                public void onResponse(Object response) {
                    progressDialog.dismiss();
                    dismiss();
                }

                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    dismiss();
                    if (listener != null) {
                        listener.onYes(message);
                    }
                }
            });
        }

        if (v == cancel) {
            dismiss();
        }
    }

    private TextWatcher countWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            charsCount.setText(String.format("%s/400", content.getText().length()));
        }
    };

    public ReplayCommentDialog setCommentId(String commentId) {
        this.commentId = commentId;
        return this;
    }

    public ReplayCommentDialog setConfessionalId(String confessionalId) {
        this.confessionalId = confessionalId;
        return this;
    }

    public ReplayCommentDialog setListener(ButtonClick listener) {
        this.listener = listener;
        return this;
    }

    public interface ButtonClick {
        void onYes(String message);
        void onCancel();
    }
}