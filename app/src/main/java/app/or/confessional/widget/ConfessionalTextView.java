package app.or.confessional.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import app.or.confessional.R;

/**
 * Created by RESIDOVI on 4/19/2018.
 */

public class ConfessionalTextView extends AppCompatTextView{
    public ConfessionalTextView(Context context) {
        super(context);
    }

    public ConfessionalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont("", attrs);
    }

    public ConfessionalTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont("", attrs);
    }

    private void setFont(String fontPath, AttributeSet attrs) {
        if (fontPath.isEmpty()) {
            fontPath = getContext().getString(R.string.slackey_font);
        }

        if (isInEditMode()) {
            return;
        }

        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.ConfessionalTextView);
        String path = array.getString(R.styleable.ConfessionalTextView_customFont);
        if (path != null) {
            fontPath = path;
        }

        AssetManager assets = getContext().getAssets();
        Typeface typeface = Typeface.createFromAsset(assets, fontPath);
        setTypeface(typeface);

        array.recycle();
    }
}
