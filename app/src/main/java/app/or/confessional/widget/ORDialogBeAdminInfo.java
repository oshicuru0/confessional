package app.or.confessional.widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import app.or.confessional.R;

public class ORDialogBeAdminInfo extends BaseDialog<ORDialogBeAdminInfo> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.90f;
    private ButtonClick listener;

    public ORDialogBeAdminInfo(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public ORDialogBeAdminInfo(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    public ORDialogBeAdminInfo(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.dialog_layout_be_admin, null);
        inflate.findViewById(R.id.ok).setOnClickListener(this);
    }

    public static ORDialogBeAdminInfo createDialog(Activity activity) {
        return new ORDialogBeAdminInfo(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    public interface ButtonClick {
        void onYes();
        void onCancel();
    }
}