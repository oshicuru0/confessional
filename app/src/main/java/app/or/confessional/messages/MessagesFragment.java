package app.or.confessional.messages;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.activities.MainActivity;

@EFragment(R.layout.fragment_messages)
public class MessagesFragment extends Fragment {

    @ViewById
    public RecyclerView recycler;

    public MessagesFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

    }
}
