package app.or.confessional.help;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.activities.MainActivity;

@EFragment(R.layout.fragment_help)
public class HelpFragment extends Fragment {

    @ViewById
    public RecyclerView recycler;

    public HelpFragment() {
    }

    @AfterViews
    public void onCreate() {
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

    }
}
