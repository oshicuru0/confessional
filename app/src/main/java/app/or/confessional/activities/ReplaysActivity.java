package app.or.confessional.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.widget.ConfessionalTextView;
import app.or.confessional.widget.ReplayCommentDialog;


/**
 * Created by osman on 4/22/2018.
 */

@EActivity(R.layout.replays_activity)
public class ReplaysActivity extends AppCompatActivity implements View.OnClickListener {

    @ViewById
    public View root;

    @ViewById
    public View placeholder;

    @ViewById
    public View parent;

    @ViewById
    public View appBar;

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public ConfessionalTextView title;

    @ViewById
    public ProgressBar progress;

    private ReplayAdapter replayAdapter;

    @Override
    public void onClick(View view) {

    }

    @AfterViews
    public void onCreate() {
        replayAdapter = new ReplayAdapter();
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(replayAdapter);
        getComments();
    }

    @Click(resName = "back")
    protected void onBack() {
        onBackPressed();
    }

    @Click(resName = "add")
    protected void add() {
        ReplayCommentDialog.createDialog(this)
                .setCommentId(commentId)
                .setConfessionalId(confessionalId)
                .setListener(new ReplayCommentDialog.ButtonClick() {
                    @Override
                    public void onYes(String message) {
                        Snackbar.make(parent, message, Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }

    private String commentId, confessionalId, replyCommentId;
    private Boolean fromNotification = false;

    private void getComments() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        commentId = bundle.getString("commentId");
        replyCommentId = bundle.getString("replyCommentId");
        replayAdapter.setCommentId(commentId);
        replayAdapter.setSelectedCommentId(replyCommentId);
        confessionalId = bundle.getString("confessionalId");
        fromNotification = bundle.getBoolean("fromNotification");

        if (commentId == null) {
            finish();
        }

        CommentHelper.getAllReplays(commentId, new CommentHelper.CommentHandler() {
            @Override
            public void onResponse(Object response) {
                progress.setVisibility(View.GONE);
                List<Comment> list = (List<Comment>) response;
                if (list != null) {
                    if (list.isEmpty()) {
                        placeholder.setVisibility(View.VISIBLE);
                    } else {
                        placeholder.setVisibility(View.GONE);
                    }
                    replayAdapter.setComments(list);
                    for (int i = 0; i < list.size(); i++) {
                        Comment comment = list.get(i);
                        if (comment != null && comment.getId().equals(replyCommentId)) {
                            recycler.smoothScrollToPosition(i);
                        }
                    }
                }
            }

            @Override
            public void onError(String message) {
                progress.setVisibility(View.GONE);
                placeholder.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (fromNotification) {
            StorageHelper.initializeUserData(this);
            Intent intent = new Intent(this, CommentsActivity_.class);
            intent.putExtra("type", "Feed");
            intent.putExtra("confessionalId", confessionalId);
            startActivity(intent);
        }

        finish();
    }
}
