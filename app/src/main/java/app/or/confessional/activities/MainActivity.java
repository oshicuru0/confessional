package app.or.confessional.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.beadmin.BeAdminFragment_;
import app.or.confessional.bestvoted.BestVotedFragment_;
import app.or.confessional.feed.FeedFragment_;
import app.or.confessional.help.HelpFragment_;
import app.or.confessional.messages.MessagesFragment_;
import app.or.confessional.mostpopular.MostPopularFragment_;
import app.or.confessional.myconfessions.MyConfessionsFragment_;
import app.or.confessional.settings.SettingsFragment_;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ConfessionalTextView;
import app.or.confessional.worstvoted.WorstVotedFragment_;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements SlideMenu.ViewAnimatorListener {

    @ViewById
    public BottomNavigationView navigation;

    @ViewById
    public ConfessionalTextView title;

    @ViewById
    public ImageButton sort;

    @ViewById
    public ImageButton hamburger;

    @ViewById
    public LinearLayout leftContainer;

    @ViewById
    public View parent;

    @ViewById
    public View appBar;

    public MainButtonClick mainButtonClick;

    public FeedFragment_ feedFragment;
    public MostPopularFragment_ mostPopularFragment;
    public BestVotedFragment_ bestVotedFragment;
    public WorstVotedFragment_ worstVotedFragment_;
    public MyConfessionsFragment_ myConfessionsFragment_;
    public MessagesFragment_ messagesFragment_;
    public BeAdminFragment_ beAdminFragment_;
    public SettingsFragment_ settingsFragment_;
    public HelpFragment_ helpFragment_;

    public SlideMenu slideMenu;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_most:
                    hideMenu();
                    changeFragment(new MostPopularFragment_(), R.string.most_popular);
                    return true;
                case R.id.navigation_feed:
                    hideMenu();
                    changeFragment(new FeedFragment_(), R.string.feed);
                    return true;
                case R.id.navigation_best_voted:
                    hideMenu();
                    changeFragment(new BestVotedFragment_(), R.string.best_voted);
                    return true;
                case R.id.navigation_worst_voted:
                    hideMenu();
                    changeFragment(new WorstVotedFragment_(), R.string.worst_voted);
                    return true;
            }
            return false;
        }
    };

    @AfterViews
    protected void onCreate() {
        UserData.getInstance().setActivity(MainActivity.this);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        slideMenu = new SlideMenu(leftContainer, this, this, R.layout.menu_list_item, R.id.menu_item_image);
        createMenuList();

        changeFragment(new FeedFragment_(), R.string.feed);
    }

    private void createMenuList() {
        if (UserData.getInstance().getUser().getUserType().equals("guest")) {
            slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("Settings", R.drawable.ic_settings));
            slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("Login", R.drawable.ic_exit));
            return;
        }

        slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("My\nconfessions", R.drawable.ic_my_confess));
        slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("Be\nAdmin", R.drawable.ic_warning));
        slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("Settings", R.drawable.ic_settings));
        slideMenu.addMenuItem(new SlideMenu.SlideMenuItem("Logout", R.drawable.ic_exit));
    }


    public void changeFragment(Fragment fragment, int title) {
        sort.setVisibility(View.GONE);
        if (fragment instanceof FeedFragment_) {
            sort.setImageResource(R.drawable.ic_add);
            sort.setVisibility(View.VISIBLE);
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (feedFragment == null) {
                feedFragment = (FeedFragment_) fragment;
            } else {
                fragment = feedFragment;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);
            navigation.getMenu().setGroupCheckable(0, true, true);
            slideMenu.setLastPosition(0);
        }

        if (fragment instanceof MostPopularFragment_) {
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMostPopular));
            if (mostPopularFragment == null) {
                mostPopularFragment = (MostPopularFragment_) fragment;
            } else {
                fragment = mostPopularFragment;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMostPopular));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorMostPopularDark));

            slideMenu.setPrimaryColor(R.color.colorMostPopular);
            slideMenu.setPrimaryDarkColor(R.color.colorMostPopularDark);
            navigation.getMenu().setGroupCheckable(0, true, true);
            slideMenu.setLastPosition(0);
        }

        if (fragment instanceof BestVotedFragment_) {
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBestVoted));
            if (bestVotedFragment == null) {
                bestVotedFragment = (BestVotedFragment_) fragment;
            } else {
                fragment = bestVotedFragment;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBestVoted));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBestVotedDark));

            slideMenu.setPrimaryColor(R.color.colorBestVoted);
            slideMenu.setPrimaryDarkColor(R.color.colorBestVotedDark);
            navigation.getMenu().setGroupCheckable(0, true, true);
            slideMenu.setLastPosition(0);
        }

        if (fragment instanceof WorstVotedFragment_) {
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWorstVoted));
            if (worstVotedFragment_ == null) {
                worstVotedFragment_ = (WorstVotedFragment_) fragment;
            } else {
                fragment = worstVotedFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWorstVoted));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorWorstVotedDark));

            slideMenu.setPrimaryColor(R.color.colorWorstVoted);
            slideMenu.setPrimaryDarkColor(R.color.colorWorstVotedDark);
            navigation.getMenu().setGroupCheckable(0, true, true);
            slideMenu.setLastPosition(0);
        }

        if (fragment instanceof MyConfessionsFragment_) {
            sort.setImageResource(R.drawable.ic_add);
            sort.setVisibility(View.VISIBLE);
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (myConfessionsFragment_ == null) {
                myConfessionsFragment_ = (MyConfessionsFragment_) fragment;
            } else {
                fragment = myConfessionsFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);

            navigation.getMenu().setGroupCheckable(0, false, true);
        }

        if (fragment instanceof MessagesFragment_) {
            if (messagesFragment_ == null) {
                messagesFragment_ = (MessagesFragment_) fragment;
            } else {
                fragment = messagesFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);

            navigation.getMenu().setGroupCheckable(0, false, true);
        }

        if (fragment instanceof BeAdminFragment_) {
            sort.setImageResource(R.drawable.ic_info);
            sort.setVisibility(View.VISIBLE);
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (beAdminFragment_ == null) {
                beAdminFragment_ = (BeAdminFragment_) fragment;
            } else {
                fragment = beAdminFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);

            navigation.getMenu().setGroupCheckable(0, false, true);
        }

        if (fragment instanceof SettingsFragment_) {
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (settingsFragment_ == null) {
                settingsFragment_ = (SettingsFragment_) fragment;
            } else {
                fragment = settingsFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);

            navigation.getMenu().setGroupCheckable(0, false, true);
        }

        if (fragment instanceof HelpFragment_) {
            navigation.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (helpFragment_ == null) {
                helpFragment_ = (HelpFragment_) fragment;
            } else {
                fragment = helpFragment_;
            }

            appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            slideMenu.setPrimaryColor(R.color.colorPrimary);
            slideMenu.setPrimaryDarkColor(R.color.colorPrimaryDark);

            navigation.getMenu().setGroupCheckable(0, false, true);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commitAllowingStateLoss();
        setTitle(title);
    }

    @Override
    public void setTitle(int title) {
        this.title.setText(title);
    }

    @Click(resName = "sort")
    protected void onSortClick() {
        if (mainButtonClick != null) {
            mainButtonClick.onSort();
        }
    }

    @Click(resName = "hamburger")
    protected void onHamburgerClick() {
        if (slideMenu.isShown()) {
            hideMenu();
        } else {
            showMenu();
        }
    }

    private void hideMenu() {
        leftContainer.setVisibility(View.GONE);
        leftContainer.removeAllViews();
        leftContainer.invalidate();
        slideMenu.hideMenuContent();
    }

    private void showMenu() {
        leftContainer.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack50));
        slideMenu.setParentHeight(parent.getHeight());
        leftContainer.setVisibility(View.VISIBLE);
        slideMenu.showMenuContent();
    }

    @Click(resName = "leftContainer")
    protected void onLeftContainer() {
        hideMenu();
    }

    public void setMainButtonClick(MainButtonClick mainButtonClick) {
        this.mainButtonClick = mainButtonClick;
    }

    @Override
    public void onSwitch(int position) {
        slideMenu.hideMenuContent();
        leftContainer.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTransparent));
        if (UserData.getInstance().getUser().getUserType().equals("guest")) {
            if (position == 0) {
                changeFragment(new SettingsFragment_(), R.string.settings);
            }
            if (position == 1) {
                finish();
                LoginHelper.removeNotificationToken();
                LoginHelper.removeGuestNotificationToken(MainActivity.this);
                startActivity(new Intent(MainActivity.this, LoginActivity_.class));
            }
            return;
        }

        if (position == 0) {
            changeFragment(new MyConfessionsFragment_(), R.string.my_confessions);
        }
        if (position == 1) {
            changeFragment(new BeAdminFragment_(), R.string.be_admin);
        }
        if (position == 2) {
            changeFragment(new SettingsFragment_(), R.string.settings);
        }
        if (position == 3) {
            LoginHelper.removeGuestNotificationToken(MainActivity.this);
            LoginHelper.removeNotificationToken();
            StorageHelper.deleteUserData(MainActivity.this);
            startActivity(new Intent(MainActivity.this, LoginActivity_.class));
            finish();
        }
    }

    @Override
    public void disableHomeButton() {

    }

    @Override
    public void enableHomeButton() {
        hideMenu();
    }

    @Override
    public void addViewToContainer(View view) {
        leftContainer.addView(view);
    }

    public interface MainButtonClick {
        void onSort();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserData.getInstance().setMainRun(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserData.getInstance().setMainRun(true);
    }
}
