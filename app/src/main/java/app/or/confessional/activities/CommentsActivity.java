package app.or.confessional.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardedVideoAd;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.AdMobHelper;
import app.or.confessional.utils.BitmapHelper;
import app.or.confessional.utils.ConfessionalShowcaseHelper;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.CommentDialog;
import app.or.confessional.widget.ConfessionalTextView;
import app.or.confessional.widget.ORProgressDialog;


/**
 * Created by osman on 4/22/2018.
 */

@EActivity(R.layout.cooments_activity)
public class CommentsActivity extends AppCompatActivity implements View.OnClickListener, CommentAdapter.OnChatItemClick, CommentAdapter.CommentListener, CommentAdapter.DownloadHandler {

    @ViewById
    public View root;

    @ViewById
    public View placeholder;

    @ViewById
    public View parent;

    @ViewById
    public View appBar;

    @ViewById
    public RecyclerView recycler;

    @ViewById
    public ConfessionalTextView title;

    @ViewById
    public ImageButton download;

    @ViewById
    public ImageButton share;

    private CommentAdapter commentAdapter;

    private Confession confession;

    private InterstitialAd interstitialAd;
    private RewardedVideoAd rewardedVideoAd;

    @Override
    public void onClick(View view) {

    }

    @AfterViews
    public void onCreate() {
        StorageHelper.intGuestUser(CommentsActivity.this);
        commentAdapter = new CommentAdapter();
        commentAdapter.setOnChatItemClick(this);
        commentAdapter.setCommentListener(this);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(commentAdapter);
        commentAdapter.setDownloadHandler(this);
        adjustLayout();
        showForAdd();
        interstitialAd = AdMobHelper.showAdd(this);
        //rewardedVideoAd = AdMobHelper.showAddReward(this);
    }

    @UiThread(delay = 120)
    public void showForAdd() {
        ConfessionalShowcaseHelper.showDownload(CommentsActivity.this);
    }

    private void adjustLayout() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String type = bundle.getString("type");
            switch (type) {
                case "Feed": {
                    appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                    commentAdapter.setPrimaryColorDark(R.color.colorPrimaryDark);
                    commentAdapter.setPrimaryColor(R.color.colorTransparent);
                    getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                    title.setText(R.string.feed);
                    break;
                }
                case "MostPopular": {
                    appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMostPopular));
                    commentAdapter.setPrimaryColorDark(R.color.colorMostPopularDark);
                    commentAdapter.setPrimaryColor(R.color.colorMostPopular50);
                    getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorMostPopularDark));
                    title.setText(R.string.most_popular);
                    break;
                }
                case "BestVoted": {
                    commentAdapter.setPrimaryColorDark(R.color.colorBestVotedDark);
                    commentAdapter.setPrimaryColor(R.color.colorBestVoted50);
                    appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBestVoted));
                    getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBestVotedDark));
                    title.setText(R.string.best_voted);
                    break;
                }
                case "WorstVoted": {
                    commentAdapter.setPrimaryColorDark(R.color.colorWorstVotedDark);
                    commentAdapter.setPrimaryColor(R.color.colorWorstVoted50);
                    appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWorstVoted));
                    getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorWorstVotedDark));
                    title.setText(R.string.worst_voted);
                    break;
                }
                case "MyConfessions": {
                    appBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                    commentAdapter.setPrimaryColorDark(R.color.colorPrimaryDark);
                    commentAdapter.setPrimaryColor(R.color.colorTransparent);
                    getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                    title.setText(R.string.my_confessions);
                    break;
                }
                default:
                    break;
            }

            String confessionId = bundle.getString("confessionalId");
            final String commentId = bundle.getString("commentId");
            final ORProgressDialog dialog = ORProgressDialog.createDialog(CommentsActivity.this);
            dialog.show();
            FeedHelper.getFeedById(confessionId, new FeedHelper.FeedHandler() {
                @Override
                public void onResponse(Object response) {
                    dialog.dismiss();
                    confession = (Confession) response;
                    commentAdapter.setConfession(confession);
                    updateData(commentId);
                }
            });

            FeedHelper.addView(confessionId);
        }
    }

    private void updateData(final String commentId) {
        CommentHelper.getAllComments(confession.getId(), new CommentHelper.CommentHandler() {
            @Override
            public void onResponse(Object response) {
                List<Comment> list = (List<Comment>) response;
                commentAdapter.setComments(list, commentId);
                for (int i = 0; i < list.size(); i++) {
                    Comment comment = list.get(i);
                    if (comment != null && comment.getId().equals(commentId)) {
                        recycler.smoothScrollToPosition(i);
                    }
                }
            }

            @Override
            public void onError(String message) {
                Snackbar.make(parent, message, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Click(resName = "back")
    protected void onBack() {
        onBackPressed();
    }

    @Click(resName = "onComment")
    protected void onComment() {
        CommentDialog.createDialog(this).setData(confession).setListener(new CommentDialog.ButtonClick() {
            @Override
            public void onYes(String message) {
                if (message != null) {
                    Snackbar.make(parent, message, Snackbar.LENGTH_LONG).show();
                }

                if (interstitialAd != null) {
                    interstitialAd.show();
                }
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

    @Override
    public void onItemClick(String commentId) {
        Intent intent = new Intent(CommentsActivity.this, ReplaysActivity_.class);
        intent.putExtra("commentId", commentId);
        intent.putExtra("confessionalId", confession.getId());
        startActivity(intent);
    }

    @Override
    public void onDownload() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            ORProgressDialog orProgressDialog = ORProgressDialog.createDialog(CommentsActivity.this);
            orProgressDialog.show();
            BitmapHelper.createBitmap(confession, CommentsActivity.this, null, commentAdapter.getPrimaryColor());
            orProgressDialog.hide();
            Snackbar.make(parent, "Confession downloaded to gallery", Snackbar.LENGTH_SHORT).show();
            interstitialAd.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1001);
        }

    }

    private String formatDate(Long timestamp) {
        Long currentTime = System.currentTimeMillis();
        long resultTime = currentTime - timestamp;
        int hours = (int) ((resultTime / (1000 * 60 * 60)) % 24);
        int minutes = (int) ((resultTime / (1000 * 60)));
        int seconds = (int) ((resultTime / (1000)));
        int days = (int) ((resultTime / (1000 * 60 * 60 * 24)));

        if (days > 0) {
            return String.format("%sd", days);
        } else if (hours > 0) {
            return String.format("%sh", hours);
        } else if (minutes > 0) {
            return String.format("%sm", minutes);
        } else if (seconds > 0) {
            return String.format("%ss", seconds);
        }

        return String.format("%ss", seconds);
    }

    protected void onShare() {
        Tools.shareConfession(confession, CommentsActivity.this);
    }

    @Override
    public void onBackPressed() {
        if (!UserData.getInstance().isMainRun()) {
            StorageHelper.initializeUserData(this);
            startActivity(new Intent(this, MainActivity_.class));
        }

        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1001 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            ORProgressDialog orProgressDialog = ORProgressDialog.createDialog(CommentsActivity.this);
            orProgressDialog.show();
            BitmapHelper.createBitmap(confession, CommentsActivity.this, null, commentAdapter.getPrimaryColor());
            orProgressDialog.hide();
            Snackbar.make(parent, "Confession downloaded to gallery", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCommentClicked() {
        onComment();
    }
}
