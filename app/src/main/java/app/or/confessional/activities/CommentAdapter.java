package app.or.confessional.activities;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.helpers.FeedHelper;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ConfessionalTextView;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Comment> comments = new ArrayList<>();
    private OnChatItemClick onChatItemClick;
    private Confession confession = null;
    private CommentListener commentListener;
    private int primaryColor = R.color.colorPrimary;
    private int primaryColorDark = R.color.colorPrimaryDark;
    private String selectedCommentId;
    private DownloadHandler downloadHandler;

    public void addData(List<Comment> chatRooms) {
        this.comments = chatRooms;
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public ConfessionalTextView content, time, upVote, downVote, replays;
        public View downVoteBox, upVoteBox, bg;

        public InboxViewHolder(View view) {
            super(view);
            itemView = view;
            content = view.findViewById(R.id.content);
            time = view.findViewById(R.id.time);
            upVote = view.findViewById(R.id.upVote);
            downVote = view.findViewById(R.id.downVote);
            downVoteBox = view.findViewById(R.id.downVoteBox);
            upVoteBox = view.findViewById(R.id.upVoteBox);
            replays = view.findViewById(R.id.replays);
            bg = view.findViewById(R.id.bg);
        }
    }

    public class InboxViewHolderHeader extends RecyclerView.ViewHolder {
        public View itemView;
        public View upVote, downVote, comment, background, commentBox;
        public ConfessionalTextView downVoteLabel, upVoteLabel, content, comments, time, views;
        public ImageButton download;

        public InboxViewHolderHeader(View view) {
            super(view);
            itemView = view;

            upVote = view.findViewById(R.id.upVoteBox);
            upVoteLabel = view.findViewById(R.id.upVote);


            downVote = view.findViewById(R.id.downVoteBox);
            downVoteLabel = view.findViewById(R.id.downVote);


            content = view.findViewById(R.id.content);
            comments = view.findViewById(R.id.comments);
            time = view.findViewById(R.id.time);
            views = view.findViewById(R.id.views);
            comment = view.findViewById(R.id.comment);

            background = view.findViewById(R.id.background);
            commentBox = view.findViewById(R.id.commentBox);

            download = view.findViewById(R.id.download);
        }
    }


    public CommentAdapter() {
        this.comments = new ArrayList<>();
    }

    public void setComments(List<Comment> comments, String selectedCommentId) {
        if (comments == null) {
            this.comments = new ArrayList<>();
        } else {
            this.comments = comments;
        }
        this.selectedCommentId = selectedCommentId;
        this.comments.add(0, null);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item_header, parent, false);
            return new InboxViewHolderHeader(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
            return new InboxViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderTmp, final int position) {
        if (holderTmp instanceof InboxViewHolderHeader) {
            final InboxViewHolderHeader holder = (InboxViewHolderHeader) holderTmp;
            holder.content.setText(confession.getConfession());
            holder.time.setText(formatDate(confession.getTimestamp()));
            holder.comments.setText(String.format("%s", String.valueOf(comments.size() - 1)));
            confession.setNumOfViews(confession.getNumOfViews());
            holder.views.setText(String.valueOf(confession.getNumOfViews()));
            holder.background.setBackgroundColor(ContextCompat.getColor(holder.background.getContext(), primaryColor));
            holder.commentBox.setBackgroundColor(ContextCompat.getColor(holder.background.getContext(), primaryColorDark));

            Integer[] data = confession.getConfessionInfo().get();

            holder.downVoteLabel.setText(String.valueOf(data[1]));
            holder.upVoteLabel.setText(String.valueOf(data[0]));

            Object value = confession.getConfessionInfo().getData().get(UserData.getInstance().getUser().getUserId());
            if (value != null) {
                if (Boolean.valueOf(String.valueOf(value))) {
                    holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_disabled, 0, 0, 0);
                    holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

                    holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorAccent));
                    holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
                } else {
                    holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
                    holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_disabled, 0, 0, 0);

                    holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
                    holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorAccent));
                }
            } else {
                holder.upVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign, 0, 0, 0);
                holder.downVoteLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign, 0, 0, 0);

                holder.upVoteLabel.setTextColor(ContextCompat.getColor(holder.upVoteLabel.getContext(), R.color.colorWhite));
                holder.downVoteLabel.setTextColor(ContextCompat.getColor(holder.downVoteLabel.getContext(), R.color.colorWhite));
            }


            holder.upVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FeedHelper.upVote(confession);
                }
            });

            holder.downVote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FeedHelper.downVote(confession);
                }
            });

            holder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentListener != null) {
                        commentListener.onCommentClicked();
                    }
                }
            });

            holder.download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadHandler.onDownload();
                }
            });
            return;
        }

        final InboxViewHolder holder = (InboxViewHolder) holderTmp;

        final Comment comment = getComments().get(position);

        holder.content.setText(comment.getContent());
        holder.time.setText(formatDate(comment.getTimestamp()));

        Integer[] data = comment.getCommentInfo().get();

        holder.upVote.setText(String.valueOf(data[0]));
        holder.downVote.setText(String.valueOf(data[1]));

        Object value = comment.getCommentInfo().getData().get(UserData.getInstance().getUser().getUserId());
        if (value != null) {
            if (Boolean.valueOf(String.valueOf(value))) {
                holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small_accent, 0, 0, 0);
                holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorAccent));

                holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small, 0, 0, 0);
                holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorPrimaryDark));
            } else {
                holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small_accent, 0, 0, 0);
                holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorAccent));

                holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small, 0, 0, 0);
                holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorPrimaryDark));
            }
        } else {
            holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small, 0, 0, 0);
            holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorPrimaryDark));

            holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small, 0, 0, 0);
            holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorPrimaryDark));
        }


        holder.downVoteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getOwnerId().equalsIgnoreCase(UserData.getInstance().getUser().getUserId())) {
                    return;
                }

                CommentHelper.downVote(confession, comment, null);
            }
        });

        holder.upVoteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getOwnerId().equalsIgnoreCase(UserData.getInstance().getUser().getUserId())) {
                    return;
                }

                CommentHelper.upVote(confession, comment, null);
            }
        });

        holder.replays.setText(String.format("%s Replies", comment.getNumOfReplays()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChatItemClick != null) {
                    onChatItemClick.onItemClick(comment.getId());
                }
            }
        });

        if (comment.getId().equalsIgnoreCase(selectedCommentId)) {
            Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.bounce);
            animation.setInterpolator(new BounceInterpolator());
            holder.itemView.setAnimation(animation);
        }
    }

    private String formatDate(Long timestamp) {
        Long currentTime = System.currentTimeMillis();
        long resultTime = currentTime - timestamp;
        int hours = (int) ((resultTime / (1000 * 60 * 60)) % 24);
        int minutes = (int) ((resultTime / (1000 * 60)));
        int seconds = (int) ((resultTime / (1000)));
        int days = (int) ((resultTime / (1000 * 60 * 60 * 24)));

        if (days > 0) {
            return String.format("%sd", days);
        } else if (hours > 0) {
            return String.format("%sh", hours);
        } else if (minutes > 0) {
            return String.format("%sm", minutes);
        } else if (seconds > 0) {
            return String.format("%ss", seconds);
        }

        return String.format("%ss", seconds);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setPrimaryColor(int primaryColor) {
        this.primaryColor = primaryColor;
    }

    public void setPrimaryColorDark(int primaryColorDark) {
        this.primaryColorDark = primaryColorDark;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setOnChatItemClick(OnChatItemClick onChatItemClick) {
        this.onChatItemClick = onChatItemClick;
    }

    public void setCommentListener(CommentListener commentListener) {
        this.commentListener = commentListener;
    }

    public int getPrimaryColor() {
        return primaryColor;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setConfession(Confession confession) {
        this.confession = confession;
    }

    public interface OnChatItemClick {
        void onItemClick(String commentId);
    }

    public interface CommentListener {
        void onCommentClicked();
    }

    public interface DownloadHandler {
        void onDownload();
    }

    public void setDownloadHandler(DownloadHandler downloadHandler) {
        this.downloadHandler = downloadHandler;
    }
}