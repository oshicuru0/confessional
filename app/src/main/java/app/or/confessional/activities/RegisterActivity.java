package app.or.confessional.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.Tools;
import app.or.confessional.widget.ORProgressDialog;

/**
 * Created by RESIDOVI on 4/26/2018.
 */

@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {

    @ViewById
    public TextInputEditText username;

    @ViewById
    public TextInputEditText password;

    @ViewById
    public View root;

    @AfterViews
    public void onCreate() {

    }

    @Click(resName = "login")
    protected void onLogin() {
        finish();
    }

    @Click(resName = "register")
    protected void onRegister() {
        if (username.getText().length() < 5) {
            username.setError("Username must have at least 5 characters!");
            username.requestFocus();
            return;
        }

        if (password.getText().length() < 5) {
            password.setError("Password must have at least 5 characters!");
            password.requestFocus();
            return;
        }

        Tools.isInternetAvailable(RegisterActivity.this, new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    handleRegister();
                } else {
                    Snackbar.make(root, "Do not have internet connection!", Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }

    private void handleRegister() {
        final ORProgressDialog progressDialog = ORProgressDialog.createDialog(this);
        progressDialog.show();
        LoginHelper.createUser(username.getText().toString(), password.getText().toString(), new LoginHelper.LoginHandler() {
            @Override
            public void onSuccess() {
                StorageHelper.refreshUserData(RegisterActivity.this);
                startActivity(new Intent(RegisterActivity.this, MainActivity_.class));
                progressDialog.dismiss();
            }

            @Override
            public void onError(String message) {
                Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
