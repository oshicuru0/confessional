package app.or.confessional.activities;


import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import app.or.confessional.resources.model.User;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;

import static app.or.confessional.utils.FirebaseConstants.USERS;
import static app.or.confessional.utils.FirebaseConstants.USERS_PASSWORD;

/**
 * Created by RESIDOVI on 4/26/2018.
 */

public class LoginHelper {
    public static void login(final String username, final String password, final LoginHandler handler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(USERS)
                .child(String.valueOf(username.hashCode()))
                .child(USERS_PASSWORD)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                           if (String.valueOf(password.hashCode()).equals(dataSnapshot.getValue(String.class))) {
                                if (handler != null) {
                                    UserData.getInstance().getUser().setUserId(String.valueOf(username.hashCode()));
                                    refreshNotificationToken();
                                    handler.onSuccess();
                                }
                            }  else {
                               if (handler != null) {
                                   handler.onError("Wrong password for this user!");
                               }
                           }
                        } else {
                            if (handler != null) {
                                handler.onError("User with '" + username + "' does not exists!");
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (handler != null) {
                            handler.onError("User with '" + username + "' does not exists!");
                        }
                    }
                });
    }

    public static void createUser(final String username, final String password, final LoginHandler handler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(USERS)
                .child(String.valueOf(username.hashCode()))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            if (handler != null) {
                                handler.onError("User with '" + username + "' already exists!");
                            }
                        } else {
                            User user = new User();
                            user.setUserId(String.valueOf(username.hashCode()));
                            user.setPassword(String.valueOf(password.hashCode()));
                            user.setUsername(username);
                            FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child(USERS)
                                    .child(String.valueOf(username.hashCode())).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (handler != null) {
                                        UserData.getInstance().getUser().setUserId(String.valueOf(username.hashCode()));
                                        refreshNotificationToken();
                                        handler.onSuccess();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public static void refreshNotificationToken() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(USERS)
                .child(UserData.getInstance().getUser().getUserId())
                .child("notificationToken")
                .setValue(FirebaseInstanceId.getInstance().getToken());
    }

    public static void removeNotificationToken() {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(USERS)
                .child(UserData.getInstance().getUser().getUserId())
                .child("notificationToken")
                .setValue(null);
    }

    public static void removeGuestNotificationToken(Activity activity) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(USERS)
                .child(Tools.getUniqueIdentification(activity))
                .child("notificationToken")
                .setValue(null);
    }

    public interface LoginHandler {
        void onSuccess();
        void onError(String message);
    }
}
