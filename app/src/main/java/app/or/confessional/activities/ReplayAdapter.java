package app.or.confessional.activities;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.resources.helpers.CommentHelper;
import app.or.confessional.resources.model.Comment;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ConfessionalTextView;


public class ReplayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Comment> comments = new ArrayList<>();
    private OnChatItemClick onChatItemClick;
    private String commentId, selectedCommentId;

    public void addData(List<Comment> chatRooms) {
        this.comments = chatRooms;
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public ConfessionalTextView content, time, upVote, downVote, replays;
        public View downVoteBox, upVoteBox;

        public InboxViewHolder(View view) {
            super(view);
            itemView = view;
            content = view.findViewById(R.id.content);
            time = view.findViewById(R.id.time);
            upVote = view.findViewById(R.id.upVote);
            downVote = view.findViewById(R.id.downVote);
            downVoteBox = view.findViewById(R.id.downVoteBox);
            upVoteBox = view.findViewById(R.id.upVoteBox);
            replays = view.findViewById(R.id.replays);
        }
    }

    public ReplayAdapter() {
        this.comments = new ArrayList<>();
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        return new InboxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderTmp, final int position) {
        final InboxViewHolder holder = (InboxViewHolder) holderTmp;
        holder.replays.setVisibility(View.GONE);
        final Comment comment = getComments().get(position);

        holder.content.setText(comment.getContent());
        holder.time.setText(formatDate(comment.getTimestamp()));

        Integer[] data = comment.getCommentInfo().get();

        holder.upVote.setText(String.valueOf(data[0]));
        holder.downVote.setText(String.valueOf(data[1]));

        Object value = comment.getCommentInfo().getData().get(UserData.getInstance().getUser().getUserId());
        if (value != null) {
            if (Boolean.valueOf(String.valueOf(value))) {
                holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small_accent, 0, 0, 0);
                holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorAccent));

                holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small, 0, 0, 0);
                holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorPrimaryDark));
            } else {
                holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small_accent, 0, 0, 0);
                holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorAccent));

                holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small, 0, 0, 0);
                holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorPrimaryDark));
            }
        } else {
            holder.upVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_sign_small, 0, 0, 0);
            holder.upVote.setTextColor(ContextCompat.getColor(holder.upVote.getContext(), R.color.colorPrimaryDark));

            holder.downVote.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_sign_small, 0, 0, 0);
            holder.downVote.setTextColor(ContextCompat.getColor(holder.downVote.getContext(), R.color.colorPrimaryDark));
        }
        holder.downVoteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getOwnerId().equalsIgnoreCase(UserData.getInstance().getUser().getUserId())) {
                    return;
                }

                CommentHelper.replayDownVote(commentId, comment);
            }
        });

        holder.upVoteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getOwnerId().equalsIgnoreCase(UserData.getInstance().getUser().getUserId())) {
                    return;
                }

                CommentHelper.replayUpVote(commentId, comment);
            }
        });

        holder.replays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onChatItemClick != null) {
                    onChatItemClick.onItemClick(comment.getId());
                }
            }
        });

        if (comment.getId().equalsIgnoreCase(selectedCommentId)) {
            Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.bounce);
            animation.setInterpolator(new BounceInterpolator());
            holder.itemView.setAnimation(animation);
        }
    }

    private String formatDate(Long timestamp) {
        Long currentTime = System.currentTimeMillis();
        long resultTime = currentTime - timestamp;
        int hours = (int) ((resultTime / (1000 * 60 * 60)) % 24);
        int minutes = (int) ((resultTime / (1000 * 60)));
        int seconds = (int) ((resultTime / (1000)));
        int days = (int) ((resultTime / (1000 * 60 * 60 * 24)));

        if (days > 0) {
            return String.format("%sd", days);
        } else if (hours > 0) {
            return String.format("%sh", hours);
        } else if (minutes > 0) {
            return String.format("%sm", minutes);
        } else if (seconds > 0) {
            return String.format("%ss", seconds);
        }

        return String.format("%ss", seconds);
    }

    public void setSelectedCommentId(String selectedCommentId) {
        this.selectedCommentId = selectedCommentId;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setOnChatItemClick(OnChatItemClick onChatItemClick) {
        this.onChatItemClick = onChatItemClick;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public interface OnChatItemClick {
        void onItemClick(String commentId);
    }
}