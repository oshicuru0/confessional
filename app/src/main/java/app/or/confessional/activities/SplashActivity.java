package app.or.confessional.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


import app.or.confessional.R;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ORProgressDialog;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @ViewById
    public View parent;

    @AfterViews
    protected void onCreate() {
        StorageHelper.initializeUserData(this);
        runMain();
    }

    @UiThread(delay = 700)
    protected void runMain() {
        if (UserData.getInstance().getUser().getUserId() == null) {
            startActivity(new Intent(SplashActivity.this, LoginActivity_.class));
            finish();
            return;
        }

        final Snackbar snackbar = Snackbar.make(parent, "You do not have internet connection!", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ORProgressDialog orProgressDialog = new ORProgressDialog(SplashActivity.this);
                orProgressDialog.show();
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       Tools.isInternetAvailable(SplashActivity.this, new Tools.CheckInternet() {
                           @Override
                           public void onFinished(Boolean state) {
                               orProgressDialog.dismiss();
                               if (state) {
                                   redirect();
                               } else {
                                   snackbar.dismiss();
                                   snackbar.show();
                               }
                           }
                       });
                   }
               }, 500);
            }
        });
        snackbar.setActionTextColor(ContextCompat.getColor(SplashActivity.this, R.color.colorMostPopular));

        Tools.isInternetAvailable(SplashActivity.this, new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    redirect();
                } else {
                    snackbar.show();
                }
            }
        });
    }

    private void redirect() {
        startActivity(new Intent(SplashActivity.this, MainActivity_.class));
        finish();
    }
}
