package app.or.confessional.activities;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import app.or.confessional.R;
import app.or.confessional.utils.Tools;
import app.or.confessional.widget.ConfessionalTextView;


public class SlideMenu {
    private final int ANIMATION_DURATION = 50;
    private AppCompatActivity appCompatActivity;
    private List<SlideMenuItem> menuItems = new ArrayList<>();
    private List<View> viewList = new ArrayList<>();
    private ViewAnimatorListener animatorListener;
    private int menuListItem;
    private int menuListItemId;
    private boolean isShown = false;
    private LinearLayout linearLayout;
    private int parentHeight = 0;
    private int primaryDarkColor, primaryColor;
    private int lastPosition = 0;

    public int getMenuItemsSize() {
        return menuItems.size();
    }

    public SlideMenu(LinearLayout linearLayout, AppCompatActivity activity, ViewAnimatorListener animatorListener, int menuListItem, int menuListItemId) {
        this.appCompatActivity = activity;
        this.animatorListener = animatorListener;
        this.menuListItem = menuListItem;
        this.menuListItemId = menuListItemId;
        this.linearLayout = linearLayout;
    }

    public void addMenuItem(SlideMenuItem item) {
        menuItems.add(item);
    }

    public void showMenuContent() {
        isShown = true;
        setViewsClickable(false);
        viewList.clear();
        double size = menuItems.size();
        for (int i = 0; i < size; i++) {
            View viewMenu = appCompatActivity.getLayoutInflater().inflate(menuListItem, null);
            final int finalI = i;
            viewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switchItem(finalI);
                }
            });

            viewMenu.findViewById(R.id.menu_item_container).
                    setBackgroundColor(ContextCompat.getColor(linearLayout.getContext(), primaryColor));
            if (lastPosition != 0 && lastPosition == i) {
                viewMenu.findViewById(R.id.menu_item_container).
                        setBackgroundColor(ContextCompat.getColor(linearLayout.getContext(), primaryDarkColor));
            } else {
                viewMenu.findViewById(R.id.menu_item_container).
                        setBackgroundColor(ContextCompat.getColor(linearLayout.getContext(), primaryColor));
            }
            viewMenu.findViewById(R.id.divider).setBackgroundColor(ContextCompat.getColor(linearLayout.getContext(), primaryDarkColor));

            ((ConfessionalTextView) viewMenu.findViewById(menuListItemId))
                    .setCompoundDrawablesRelativeWithIntrinsicBounds(0, menuItems.get(i).getImageRes(), 0, 0);

            ConfessionalTextView textView = ((ConfessionalTextView) viewMenu.findViewById(menuListItemId));
            textView.setText(menuItems.get(i).getName());

            viewMenu.setLayoutParams(new LinearLayout.LayoutParams(Tools.getTextWidth("MyConfessions", textView.getPaint()), (int) (parentHeight/size)));

            viewMenu.setVisibility(View.GONE);
            viewMenu.setEnabled(false);
            viewList.add(viewMenu);
            animatorListener.addViewToContainer(viewMenu);
            final double position = i;
            final double delay = 3 * ANIMATION_DURATION * (position / size);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (position < viewList.size()) {
                        animateView((int) position);
                    }
                    if (position == viewList.size() - 1) {
                        setViewsClickable(true);
                    }
                }
            }, (long) delay);
        }
    }

    public void hideMenuContent() {
        isShown = false;
        setViewsClickable(false);
        double size = menuItems.size();
        for (int i = menuItems.size(); i >= 0; i--) {
            final double position = i;
            final double delay = 3 * ANIMATION_DURATION * (position / size);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (position < viewList.size()) {
                        animateHideView((int) position);
                    }
                }
            }, (long) delay);
        }
    }

    private void setViewsClickable(boolean clickable) {
        animatorListener.disableHomeButton();
        for (View view : viewList) {
            view.setEnabled(clickable);
        }
    }

    private void animateView(int position) {
        final View view = viewList.get(position);
        view.setVisibility(View.VISIBLE);
        FlipAnimation rotation = new FlipAnimation(90, 0, 0.0f, view.getHeight() / 2.0f);
        rotation.setDuration(ANIMATION_DURATION);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(rotation);
    }

    private void animateHideView(final int position) {
        final View view = viewList.get(position);
        FlipAnimation rotation = new FlipAnimation(0, 90, 0.0f, view.getHeight() / 2.0f);
        rotation.setDuration(ANIMATION_DURATION);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
                view.setVisibility(View.INVISIBLE);
                if (position == viewList.size() - 1) {
                    animatorListener.enableHomeButton();
                    linearLayout.removeAllViews();
                    linearLayout.invalidate();
                    animatorListener.onSwitch(lastPosition);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(rotation);
    }

    public boolean isShown() {
        return isShown;
    }

    private void switchItem(int topPosition) {
        hideMenuContent();
        lastPosition = topPosition;
    }

    public void setParentHeight(int parentHeight) {
        this.parentHeight = parentHeight;
    }

    public void setPrimaryColor(int primaryColor) {
        this.primaryColor = primaryColor;
    }

    public void setPrimaryDarkColor(int primaryDarkColor) {
        this.primaryDarkColor = primaryDarkColor;
    }

    public void setLastPosition(int lastPosition) {
        this.lastPosition = lastPosition;
    }

    public interface ViewAnimatorListener {
         void onSwitch(int position);
         void disableHomeButton();
         void enableHomeButton();
         void addViewToContainer(View view);
    }

    public interface Resource {
        String getName();
    }

    public static class FlipAnimation extends Animation {
        private float fromDegrees;
        private float toDegrees;
        private float centerX;
        private float centerY;
        private Camera camera;

        FlipAnimation(float fromDegrees, float toDegrees, float centerX, float centerY) {
            this.fromDegrees = fromDegrees;
            this.toDegrees = toDegrees;
            this.centerX = centerX;
            this.centerY = centerY;
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            camera = new Camera();
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            final float fromDegrees = this.fromDegrees;
            float degrees = fromDegrees + ((toDegrees - fromDegrees) * interpolatedTime);
            Matrix matrix = t.getMatrix();
            camera.save();
            camera.rotateY(degrees);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);
        }
    }

    public static class SlideMenuItem implements Resource {
        private String name;
        private int imageRes;

        public SlideMenuItem(String name, int imageRes) {
            this.name = name;
            this.imageRes = imageRes;
        }

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public int getImageRes() {
            return imageRes;
        }
    }
}
