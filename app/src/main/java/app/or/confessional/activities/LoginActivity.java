package app.or.confessional.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.or.confessional.R;
import app.or.confessional.utils.StorageHelper;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;
import app.or.confessional.widget.ORProgressDialog;

/**
 * Created by RESIDOVI on 4/26/2018.
 */

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById
    public Button login;

    @ViewById
    public TextInputEditText username;

    @ViewById
    public TextInputEditText password;

    @ViewById
    public Button guest;

    @ViewById
    public View root;

    @AfterViews
    public void onCreate() {

    }

    @Click(resName = "guest")
    protected void onGuest() {
        Tools.isInternetAvailable(LoginActivity.this, new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    UserData.getInstance().getUser().setUserType("guest");
                    UserData.getInstance().getUser().setUserId(Tools.getUniqueIdentification(LoginActivity.this));
                    LoginHelper.refreshNotificationToken();
                    startActivity(new Intent(LoginActivity.this, MainActivity_.class));
                    finish();
                } else {
                    Snackbar.make(root, "Do not have internet connection!", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Click(resName = "register")
    protected void onRegister() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity_.class));
    }

    @Click(resName = "login")
    protected void onLogin() {
        if (username.getText().length() < 5) {
            username.setError("Username must have at least 5 characters!");
            username.requestFocus();
            return;
        }

        if (password.getText().length() < 5) {
            password.setError("Password must have at least 5 characters!");
            password.requestFocus();
            return;
        }

        Tools.isInternetAvailable(LoginActivity.this, new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    handleLogin();
                } else {
                    Snackbar.make(root, "Do not have internet connection!", Snackbar.LENGTH_LONG).show();
                }
            }
        });


    }

    private void handleLogin() {
        LoginHelper.removeGuestNotificationToken(LoginActivity.this);
        final ORProgressDialog progressDialog = ORProgressDialog.createDialog(this);
        progressDialog.show();
        LoginHelper.login(username.getText().toString(), password.getText().toString(), new LoginHelper.LoginHandler() {
            @Override
            public void onSuccess() {
                StorageHelper.refreshUserData(LoginActivity.this);
                startActivity(new Intent(LoginActivity.this, MainActivity_.class));
                finish();
                progressDialog.dismiss();
            }

            @Override
            public void onError(String message) {
                Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });
    }
}
