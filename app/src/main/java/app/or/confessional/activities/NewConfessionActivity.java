package app.or.confessional.activities;

import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import app.or.confessional.R;

@EActivity(R.layout.activity_new_confession)
public class NewConfessionActivity extends AppCompatActivity {

    @AfterViews
    protected void onCreate() {

    }
}
