package app.or.confessional.resources.model;

import com.google.firebase.database.Exclude;

/**
 * Created by RESIDOVI on 4/26/2018.
 */

public class User {
    public String userId;
    public String password;
    public String username;
    public String notificationToken;

    @Exclude
    public String userType = "guest";

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Exclude
    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Exclude
    public String getUserType() {
        return userType;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }
}
