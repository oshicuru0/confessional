package app.or.confessional.resources.model;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by RESIDOVI on 4/27/2018.
 */

public class CommentInfo implements Serializable {
    public HashMap<String, Object> data = new HashMap<>();

    public void setData(HashMap<String, Object> data) {
        if (data != null) {
            this.data = data;
        }
    }

    public HashMap<String, Object> getData() {
        if (data == null) {
            data = new HashMap<>();
        }

        return data;
    }

    @Exclude
    public Integer[] get() {
        int upVote = 0, downVote = 0;
        for (Object obj : data.values()) {
            if (Boolean.valueOf(String.valueOf(obj))) {
                upVote++;
            } else {
                downVote++;
            }
        }
        return new Integer[]{upVote, downVote};
    }
}
