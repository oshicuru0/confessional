package app.or.confessional.resources.helpers;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;

import static app.or.confessional.utils.FirebaseConstants.CONFESSIONS;
import static app.or.confessional.utils.FirebaseConstants.CONFESSIONS_OWNER_ID;
import static app.or.confessional.utils.FirebaseConstants.CONFESSIONS_VIEW;
import static app.or.confessional.utils.FirebaseConstants.CONFESSION_INFO;

/**
 * Created by RESIDOVI on 4/25/2018.
 */

public class FeedHelper {

    public static void getAllFeeds(final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild("pending")
                .equalTo(false)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot);
                                list = feedResponse.toList();
                                Collections.sort(list, new Comparator<Confession>() {
                                    @Override
                                    public int compare(Confession o1, Confession o2) {
                                        if (o1.getTimestamp() > o2.getTimestamp()) {
                                            return -1;
                                        }

                                        if (o1.getTimestamp() < o2.getTimestamp()) {
                                            return 1;
                                        }
                                        return 0;
                                    }
                                });
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getFeedById(String feedId, final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .child(feedId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            if (dataSnapshot.exists()) {
                                Confession confession = dataSnapshot.getValue(Confession.class);
                                confession.getConfessionInfo().setData((HashMap<String, Object>) dataSnapshot.child(CONFESSION_INFO).getValue());
                                feedHandler.onResponse(confession);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getAllPendingFeeds(final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild("pending")
                .equalTo(true)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot, true);
                                list = feedResponse.toList();
                                Collections.sort(list);
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getAllMostPopular(final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild("pending")
                .equalTo(false)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot);
                                list = feedResponse.toList();
                                Collections.sort(list, new Comparator<Confession>() {
                                    @Override
                                    public int compare(Confession o1, Confession o2) {
                                        if (o1.getNumOfViews() < o2.getNumOfViews()) {
                                            return 1;
                                        }

                                        if (o1.getNumOfViews() > o2.getNumOfViews()) {
                                            return -1;
                                        }
                                        return 0;
                                    }
                                });
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getAllMyConfessions(final FeedHandler feedHandler) {
        String userId = UserData.getInstance().getUser().getUserId();
        if (userId == null) {
            if (feedHandler != null) {
                feedHandler.onResponse(new ArrayList<Confession>());
            }
            return;
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild(CONFESSIONS_OWNER_ID)
                .equalTo(userId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot);
                                list = feedResponse.toList();
                                Collections.reverse(list);
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getAllBestVoted(final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild("pending")
                .equalTo(false)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot);
                                list = feedResponse.toList();
                                Collections.sort(list, new Comparator<Confession>() {
                                    @Override
                                    public int compare(Confession o1, Confession o2) {
                                        Integer[] data = o1.getConfessionInfo().get();
                                        Integer[] data1 = o2.getConfessionInfo().get();

                                        if (data[0] > data1[0]) {
                                            return -1;
                                        }

                                        if (data[0] < data1[0]) {
                                            return 1;
                                        }
                                        return 0;
                                    }
                                });
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void getAllDownVote(final FeedHandler feedHandler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .orderByChild("pending")
                .equalTo(false)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (feedHandler != null) {
                            List<Confession> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                FeedResponse feedResponse = new FeedResponse(dataSnapshot);
                                list = feedResponse.toList();
                                Collections.sort(list, new Comparator<Confession>() {
                                    @Override
                                    public int compare(Confession o1, Confession o2) {
                                        Integer[] data = o1.getConfessionInfo().get();
                                        Integer[] data1 = o2.getConfessionInfo().get();

                                        if (data[1] > data1[1]) {
                                            return -1;
                                        }

                                        if (data[1] < data1[1]) {
                                            return 1;
                                        }
                                        return 0;
                                    }
                                });
                            }

                            feedHandler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (feedHandler != null) {
                            feedHandler.onResponse(new ArrayList<Confession>());
                        }
                    }
                });
    }

    public static void postConfessional(final Confession confession, final FeedHandler feedHandler) {
        Tools.isInternetAvailable(UserData.getInstance().getActivity(), new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    String key = FirebaseDatabase.getInstance().getReference().push().getKey();
                    confession.setId(key);
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(CONFESSIONS)
                            .child(key)
                            .setValue(confession)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (feedHandler != null) {
                                        feedHandler.onResponse("Confession is posted. Waiting for verification!");
                                    }
                                }
                            });
                } else {
                    feedHandler.onResponse("Do not have internet connection!");
                }
            }
        });
    }

    public static void upVote(Confession confession) {
        confession.getConfessionInfo().getData().put(UserData.getInstance().getUser().getUserId(), true);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .child(confession.getId())
                .child(CONFESSION_INFO)
                .updateChildren(confession.getConfessionInfo().getData());

    }

    public static void downVote(Confession confession) {
        confession.getConfessionInfo().getData().put(UserData.getInstance().getUser().getUserId(), false);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .child(confession.getId())
                .child(CONFESSION_INFO)
                .updateChildren(confession.getConfessionInfo().getData());
    }

    public static void addView(final String confessionId) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(CONFESSIONS)
                .child(confessionId)
                .child(CONFESSIONS_VIEW)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        FirebaseDatabase.getInstance()
                                .getReference()
                                .child(CONFESSIONS)
                                .child(confessionId)
                                .child(CONFESSIONS_VIEW)
                                .setValue(dataSnapshot.getValue(Long.class) + 1);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public interface FeedHandler {
        void onResponse(Object response);
    }

    public static class FeedResponse {
        private List<Confession> confessions = new ArrayList<>();

        public FeedResponse(DataSnapshot dataSnapshot) {
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Confession confession = snapshot.getValue(Confession.class);
                confession.getConfessionInfo().setData((HashMap<String, Object>) snapshot.child(CONFESSION_INFO).getValue());
                confessions.add(confession);
            }
        }

        public FeedResponse(DataSnapshot dataSnapshot, boolean exclude) {
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Confession confession = snapshot.getValue(Confession.class);
                confession.getConfessionInfo().setData((HashMap<String, Object>) snapshot.child(CONFESSION_INFO).getValue());
                if (!confession.getConfessionInfo().getData().values().contains(UserData.getInstance().getUser().getUserId())) {
                    confessions.add(confession);
                }
            }
        }

        @Exclude
        public List<Confession> toList() {
            return confessions;
        }
    }
}
