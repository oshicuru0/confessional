package app.or.confessional.resources.helpers;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import app.or.confessional.resources.model.Comment;
import app.or.confessional.resources.model.Confession;
import app.or.confessional.utils.Tools;
import app.or.confessional.utils.UserData;

import static app.or.confessional.utils.FirebaseConstants.COMMENTS;
import static app.or.confessional.utils.FirebaseConstants.COMMENTS_DOWN_VOTE;
import static app.or.confessional.utils.FirebaseConstants.COMMENTS_UP_VOTE;
import static app.or.confessional.utils.FirebaseConstants.COMMENT_INFO;
import static app.or.confessional.utils.FirebaseConstants.REPLAY_COMMENTS;

/**
 * Created by RESIDOVI on 4/25/2018.
 */

public class CommentHelper {
    public static void getAllComments(String key, final CommentHandler handler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(COMMENTS)
                .child(key)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (handler != null) {
                            Collection<Comment> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                CommentResponse commentResponse = new CommentResponse(dataSnapshot);
                                list = commentResponse.toList();
                            }

                            handler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        if (handler != null) {
                            handler.onResponse(new ArrayList<>());
                        }
                    }
                });
    }

    public static void getAllReplays(String key, final CommentHandler handler) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(REPLAY_COMMENTS)
                .child(key)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (handler != null) {
                            Collection<Comment> list = new ArrayList<>();
                            if (dataSnapshot.exists()) {
                                CommentResponse commentResponse = new CommentResponse(dataSnapshot);
                                list = commentResponse.toList();
                            }

                            handler.onResponse(list);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        if (handler != null) {
                            handler.onResponse(new ArrayList<>());
                        }
                    }
                });
    }

    public static void postComment(final Confession confession, final Comment comment, final CommentHandler handler) {
        Tools.isInternetAvailable(UserData.getInstance().getActivity(), new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    String key = FirebaseDatabase.getInstance().getReference().push().getKey();
                    comment.setId(key);
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(COMMENTS)
                            .child(confession.getId())
                            .child(key)
                            .setValue(comment)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (handler != null) {
                                        handler.onResponse(task.isSuccessful());
                                    }
                                }
                            });
                } else {
                    handler.onError("Do not have internet connection!");
                }
            }
        });
    }

    public static void postReplayComment(final String confessionalId, final String commentId, final Comment comment, final CommentHandler handler) {
        Tools.isInternetAvailable(UserData.getInstance().getActivity(), new Tools.CheckInternet() {
            @Override
            public void onFinished(Boolean state) {
                if (state) {
                    String key = FirebaseDatabase.getInstance().getReference().push().getKey();
                    comment.setId(key);
                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(REPLAY_COMMENTS)
                            .child(commentId)
                            .child(key)
                            .setValue(comment)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (handler != null) {
                                        handler.onResponse(task.isSuccessful());
                                    }
                                }
                            });

                    FirebaseDatabase.getInstance()
                            .getReference()
                            .child(COMMENTS)
                            .child(confessionalId)
                            .child(commentId)
                            .child("numOfReplays")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    HashMap<String, Object> update = new HashMap<>();
                                    update.put("numOfReplays", dataSnapshot.getValue(Long.class) == null ? 0:  dataSnapshot.getValue(Long.class) + 1);
                                    FirebaseDatabase.getInstance()
                                            .getReference()
                                            .child(COMMENTS)
                                            .child(confessionalId)
                                            .child(commentId).updateChildren(update);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                } else {
                    handler.onError("Do not have internet connection!");
                }
            }
        });
    }

    public static void getCommentCount(final Confession confession, final CommentHandler handler) {
        if (confession.getId() == null) {
            return;
        }

        FirebaseDatabase.getInstance()
                .getReference()
                .child(COMMENTS)
                .child(confession.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long count = 0;
                if (dataSnapshot.exists()) {
                    count = dataSnapshot.getChildrenCount();
                }
                if (handler != null) {
                    handler.onResponse(count);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if (handler != null) {
                    handler.onResponse(0);
                }
            }
        });

    }

    public static void upVote(Confession confession, Comment comment, CommentHandler handler) {
        comment.getCommentInfo().getData().put(UserData.getInstance().getUser().getUserId(), true);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(COMMENTS)
                .child(confession.getId())
                .child(comment.getId())
                .child(COMMENT_INFO)
                .updateChildren(comment.getCommentInfo().getData());
    }

    public static void replayUpVote(String commentId, Comment comment) {
        comment.getCommentInfo().getData().put(UserData.getInstance().getUser().getUserId(), true);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(REPLAY_COMMENTS)
                .child(commentId)
                .child(comment.getId())
                .child(COMMENT_INFO)
                .updateChildren(comment.getCommentInfo().getData());
    }

    public static void replayDownVote(String commentId, Comment comment) {
        comment.getCommentInfo().getData().put(UserData.getInstance().getUser().getUserId(), false);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(REPLAY_COMMENTS)
                .child(commentId)
                .child(comment.getId())
                .child(COMMENT_INFO)
                .updateChildren(comment.getCommentInfo().getData());
    }

    public static void downVote(Confession confession, Comment comment, CommentHandler handler) {
        comment.getCommentInfo().getData().put(UserData.getInstance().getUser().getUserId(), false);
        FirebaseDatabase.getInstance()
                .getReference()
                .child(COMMENTS)
                .child(confession.getId())
                .child(comment.getId())
                .child(COMMENT_INFO)
                .updateChildren(comment.getCommentInfo().getData());
    }

    public interface CommentHandler {
        void onResponse(Object response);

        void onError(String message);
    }

    public static class CommentResponse {
        private List<Comment> comments = new ArrayList<>();

        public CommentResponse(DataSnapshot dataSnapshot) {
            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                Comment comment = snapshot.getValue(Comment.class);
                if (comment == null) {
                    continue;
                }

                comment.getCommentInfo().setData((HashMap<String, Object>) snapshot.child(COMMENT_INFO).getValue());
                comments.add(comment);
            }
        }

        @Exclude
        public List<Comment> toList() {
            return comments;
        }
    }
}
