package app.or.confessional.resources.model;

/**
 * Created by RESIDOVI on 4/25/2018.
 */

public class Comment {
    private String id;
    private String content;
    private Long timestamp;
    private String ownerId;
    private CommentInfo commentInfo = new CommentInfo();
    private Integer numOfReplays = 0;
    private String confessionalId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setCommentInfo(CommentInfo commentInfo) {
        this.commentInfo = commentInfo;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public CommentInfo getCommentInfo() {
        return commentInfo;
    }

    public void setNumOfReplays(Integer numOfReplays) {
        this.numOfReplays = numOfReplays;
    }

    public Integer getNumOfReplays() {
        return numOfReplays;
    }

    public void setConfessionalId(String confessionalId) {
        this.confessionalId = confessionalId;
    }

    public String getConfessionalId() {
        return confessionalId;
    }
}
