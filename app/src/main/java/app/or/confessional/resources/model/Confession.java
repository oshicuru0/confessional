package app.or.confessional.resources.model;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by RESIDOVI on 4/19/2018.
 */

public class Confession implements Serializable, Comparable {
    private String id;
    private String confession;
    private Long timestamp;
    private Integer numOfComments = 0;
    private Integer numOfViews = 0;
    private String ownerId;
    private Boolean pending = true;
    private ConfessionInfo confessionInfo = new ConfessionInfo();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfession() {
        return confession;
    }

    public void setConfession(String confession) {
        this.confession = confession;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNumOfComments() {
        return numOfComments;
    }

    public void setNumOfComments(Integer numOfComments) {
        this.numOfComments = numOfComments;
    }

    public Integer getNumOfViews() {
        return numOfViews;
    }

    public void setNumOfViews(Integer numOfViews) {
        this.numOfViews = numOfViews;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public void setConfessionInfo(ConfessionInfo confessionInfo) {
        this.confessionInfo = confessionInfo;
    }

    public ConfessionInfo getConfessionInfo() {
        return confessionInfo;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    public Boolean getPending() {
        return pending;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Confession newOne = (Confession) o;
        if (timestamp > newOne.getTimestamp()) {
            return -1;
        }
        if (timestamp < newOne.getTimestamp()) {
            return 1;
        }
        return 0;
    }
}
